package fr.imie.rest;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.Writer;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Servlet implementation class Rss
 */

@WebServlet({ "/Rss" })	
public class Rss extends HttpServlet {
	private static final long serialVersionUID = 1L;
	
    /**
     * @see HttpServlet#HttpServlet()
     */
    public Rss() {
        super();
        // TODO Auto-generated constructor stub
       
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		response.setHeader("Access-Control-Allow-Origin", "*");
		response.setHeader("Access-Control-Allow-Methods", "POST, GET, OPTIONS, DELETE");
		response.setHeader("Access-Control-Max-Age", "3600");
		response.setHeader("Access-Control-Allow-Headers", "x-requested-with");
		
		//request.getRequestDispatcher("index.jsp").forward(request, response);
		
		 	final String USER_AGENT = "Tempo";
	        try{
	        String url = "http://www.nantesmetropole.fr/adminsite/webservices/export_rss.jsp?OBJET=ARTICLENM&TYPE_ARTICLE=0002&THEMATIQUE=0005&NOMBRE=10&DESCRIPTION=Emploi&FLUX_RSS=1";
	        
			URL obj = new URL(url);
			HttpURLConnection connexion = (HttpURLConnection) obj.openConnection();
	 
			
			connexion.setRequestMethod("GET");
	 
			connexion.setRequestProperty("User-Agent", USER_AGENT);
	        
			int responseCode = connexion.getResponseCode();
			BufferedReader in = new BufferedReader(
			        new InputStreamReader(connexion.getInputStream()));
			String inputLine;
			StringBuffer resultat = new StringBuffer();
	 
			while ((inputLine = in.readLine()) != null) {
				System.out.println("trouver 1");
				resultat.append(inputLine);
			}
			in.close();
			
			//affichage du résultat
			Writer writer = response.getWriter();
			writer.write(resultat.toString());
			
			response.setContentType("application/xml");
			
			
	        } catch (MalformedURLException e) {
	          System.out.println("url pourrie");
	        } catch (IOException e) {
	        	 System.out.println(e);
	        }	
			
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
	}

}

