package fr.imie.rest;


import java.util.List;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import fr.cyberbase.model.Fonction;
import fr.imie.service.FonctionServiceLocal;



@Path("fonction")
@Stateless
@Produces({ MediaType.APPLICATION_JSON })
@Consumes({ MediaType.APPLICATION_JSON })
public class FonctionServiceREST {

	@EJB
	private FonctionServiceLocal fonctionService;

	@GET
	public Response getAllFonctions() {
		List<Fonction> retour = fonctionService.findAll();
		return Response.ok(retour).build();
	}
	
	@GET
	@Path("/{id}")
	public Response getOneFonctions(@PathParam("id") Integer id){
		Fonction fonction = new Fonction();
		fonction.setIdFonction(id);
		Fonction retour = fonctionService.findFonctionById(fonction);
		return Response.ok(retour).build();
	}

	@DELETE
	@Path("/{id}")
	public void deleteFonction(@PathParam("id") Integer id) {
		Fonction fonctionToDelete = new Fonction();
		fonctionToDelete.setIdFonction(id);
		fonctionService.deleteFonction(fonctionToDelete);
	}
	
	@POST
	public Response insertFonction(Fonction fonction){
		fonction = fonctionService.createFonction(fonction);
		return Response.ok(fonction).build();
	}
	
	@PUT
	@Path("/{id}")
	public Response updateFonction(Fonction fonction,@PathParam("id") Integer id){
		fonction.setIdFonction(id);
		fonction = fonctionService.updateFonction(fonction);
		return Response.ok(fonction).build();
	}

}
