package fr.imie.rest;

import java.util.List;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import fr.cyberbase.model.Employe;
import fr.imie.service.IServiceEmploye;




@Path("employe")
@Stateless
@Produces({ MediaType.APPLICATION_JSON })
@Consumes({ MediaType.APPLICATION_JSON })
public class EmployeREST {

	@EJB
	private IServiceEmploye iServEmpl;
	
	
	@GET
	public Response getAllEmployees() {
//		String retour = "test";
//		String retour = iServEmpl.champTest();
		List<Employe> retour = iServEmpl.findEmployeAll();
		return Response.ok(retour).build();
	}
	
	@GET
	@Path("/{id}")
	public Response getOnePersonnes(@PathParam("id") Integer idEmploye){
		Employe employeMDE = new Employe();
		employeMDE.setIdEmploye(idEmploye);
		Employe retour = iServEmpl.findEmployeeById(employeMDE);
		return Response.ok(retour).build();

	}

	@DELETE
	@Path("/{id}")
	public void deletePersonne(@PathParam("id") Integer idEmploye) {
		Employe employeToDelete = new Employe();
		employeToDelete.setIdEmploye(idEmploye);
		iServEmpl.deleteEmploye(employeToDelete);
	}
	
	@POST
	public Response insertEmploye(Employe employe){
		employe = iServEmpl.createPerson(employe);
		return Response.ok(employe).build();
	}
	
	@PUT
	@Path("/{id}")
	public Response updatePersonne(Employe employe,@PathParam("id") Integer idEmploye){
		employe.setIdEmploye(idEmploye);
		employe = iServEmpl.updatePerson(employe);
		return Response.ok(employe).build();
	}
	
	
}
