package fr.imie.rest;


import java.util.List;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import fr.cyberbase.model.Poste;
import fr.cyberbase.model.Utilisation;
import fr.imie.service.IUtilisationServ;
import fr.imie.service.PosteServiceLocal;


@Path("poste")
@Stateless
@Produces({ MediaType.APPLICATION_JSON })
@Consumes({ MediaType.APPLICATION_JSON })
public class PosteServiceREST {

	@EJB
	private PosteServiceLocal posteService;
	@EJB
	private IUtilisationServ IUtili;

	@GET
	public Response getAllPostes() {
		List<Poste> retour = posteService.findAll();
		return Response.ok(retour).build();

	}
	
	@GET
	@Path("/{id}")
	public Response getOnePostes(@PathParam("id") Integer id){
		Poste poste = new Poste();
		poste.setIdPoste(id);
		Poste retour = posteService.findPosteById(poste);
		return Response.ok(retour).build();

	}
	@GET
	@Path("/today/{id}")
	public Response getUtilisationAujourdhui(@PathParam("id") Integer id){
		Poste poste = new Poste();
		poste.setIdPoste(id);
		List<Utilisation> retour = IUtili.getUtilisationPourUnPostePourUneJournee(poste);
		return Response.ok(retour).build();

	}

	@DELETE
	@Path("/{id}")
	public void deletePoste(@PathParam("id") Integer id) {
		Poste posteToDelete = new Poste();
		posteToDelete.setIdPoste(id);
		posteService.deletePoste(posteToDelete);
	}
	
	@POST
	public Response insertPoste(Poste poste){
		poste = posteService.createPoste(poste);
		return Response.ok(poste).build();
	}
	
	@PUT
	@Path("/{id}")
	public Response updatePoste(Poste poste,@PathParam("id") Integer id){
		poste.setIdPoste(id);
		poste = posteService.updatePoste(poste);
		return Response.ok(poste).build();
	}

}
