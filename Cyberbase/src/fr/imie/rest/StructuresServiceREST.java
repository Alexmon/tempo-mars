package fr.imie.rest;


import java.util.List;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import fr.cyberbase.model.Structures;
import fr.imie.service.StructuresServiceLocal;



@Path("structures")
@Stateless
@Produces({ MediaType.APPLICATION_JSON })
@Consumes({ MediaType.APPLICATION_JSON })
public class StructuresServiceREST {

	@EJB
	private StructuresServiceLocal structuresService;

	@GET
	public Response getAllStructuress() {
		List<Structures> retour = structuresService.findAll();
		return Response.ok(retour).build();

	}
	
	@GET
	@Path("/{id}")
	public Response getOneStructuress(@PathParam("id") Integer id){
		Structures structures = new Structures();
		structures.setIdStructures(id);
		Structures retour = structuresService.findStructuresById(structures);
		return Response.ok(retour).build();

	}

	@DELETE
	@Path("/{id}")
	public void deleteStructures(@PathParam("id") Integer id) {
		Structures structuresToDelete = new Structures();
		structuresToDelete.setIdStructures(id);
		structuresService.deleteStructures(structuresToDelete);
	}
	
	@POST
	public Response insertStructures(Structures structures){
		structures = structuresService.createStructures(structures);
		return Response.ok(structures).build();
	}
	
	@PUT
	@Path("/{id}")
	public Response updateStructures(Structures structures,@PathParam("id") Integer id){
		structures.setIdStructures(id);
		structures = structuresService.updateStructures(structures);
		return Response.ok(structures).build();
	}

}
