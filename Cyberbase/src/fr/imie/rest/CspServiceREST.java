package fr.imie.rest;


import java.util.List;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import fr.cyberbase.model.Csp;
import fr.imie.service.CspServiceLocal;



@Path("csp")
@Stateless
@Produces({ MediaType.APPLICATION_JSON })
@Consumes({ MediaType.APPLICATION_JSON })
public class CspServiceREST {

	@EJB
	private CspServiceLocal cspService;

	@GET
	public Response getAllCsps() {
		List<Csp> retour = cspService.findAll();
		return Response.ok(retour).build();

	}
	
	@GET
	@Path("/{id}")
	public Response getOneCsps(@PathParam("id") Integer id){
		Csp csp = new Csp();
		csp.setIdCsp(id);
		Csp retour = cspService.findCspById(csp);
		return Response.ok(retour).build();

	}

	@DELETE
	@Path("/{id}")
	public void deleteCsp(@PathParam("id") Integer id) {
		Csp cspToDelete = new Csp();
		cspToDelete.setIdCsp(id);
		cspService.deleteCsp(cspToDelete);
	}
	
	@POST
	public Response insertCsp(Csp csp){
		csp = cspService.createCsp(csp);
		return Response.ok(csp).build();
	}
	
	@PUT
	@Path("/{id}")
	public Response updateCsp(Csp csp,@PathParam("id") Integer id){
		csp.setIdCsp(id);
		csp = cspService.updateCsp(csp);
		return Response.ok(csp).build();
	}

}
