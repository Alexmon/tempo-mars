package fr.imie.rest;


import java.util.List;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import fr.cyberbase.model.NiveauFormation;
import fr.imie.service.NiveauFormationServiceLocal;



@Path("nivformation")
@Stateless
@Produces({ MediaType.APPLICATION_JSON })
@Consumes({ MediaType.APPLICATION_JSON })
public class NiveauFormationServiceREST {

	@EJB
	private NiveauFormationServiceLocal niveauFormationService;

	@GET
	public Response getAllNiveauFormations() {
		List<NiveauFormation> retour = niveauFormationService.findAll();
		return Response.ok(retour).build();

	}
	
	@GET
	@Path("/{id}")
	public Response getOneNiveauFormations(@PathParam("id") Integer id){
		NiveauFormation niveauFormation = new NiveauFormation();
		niveauFormation.setidNivFormation(id);
		NiveauFormation retour = niveauFormationService.findNiveauFormationById(niveauFormation);
		return Response.ok(retour).build();
	}

	@DELETE
	@Path("/{id}")
	public void deleteNiveauFormation(@PathParam("id") Integer id) {
		NiveauFormation niveauFormationToDelete = new NiveauFormation();
		niveauFormationToDelete.setidNivFormation(id);
		niveauFormationService.deleteNiveauFormation(niveauFormationToDelete);
	}
	
	@POST
	public Response insertNiveauFormation(NiveauFormation niveauFormation){
		niveauFormation = niveauFormationService.createNiveauFormation(niveauFormation);
		return Response.ok(niveauFormation).build();
	}
	
	@PUT
	@Path("/{id}")
	public Response updateNiveauFormation(NiveauFormation niveauFormation,@PathParam("id") Integer id){
		niveauFormation.setidNivFormation(id);
		niveauFormation = niveauFormationService.updateNiveauFormation(niveauFormation);
		return Response.ok(niveauFormation).build();
	}

}
