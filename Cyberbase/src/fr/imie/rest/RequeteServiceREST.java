package fr.imie.rest;


import java.util.List;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import fr.cyberbase.model.Requete;
import fr.imie.service.RequeteServiceLocal;



@Path("requete")
@Stateless
@Produces({ MediaType.APPLICATION_JSON })
@Consumes({ MediaType.APPLICATION_JSON })
public class RequeteServiceREST {

	@EJB
	private RequeteServiceLocal requeteService;

	@GET
	public Response getAllRequetes() {
		List<Requete> retour = requeteService.findAll();
		return Response.ok(retour).build();

	}
	
	@GET
	@Path("/{id}")
	public Response getOneRequetes(@PathParam("id") Integer id){
		Requete requete = new Requete();
		requete.setIdRequete(id);
		Requete retour = requeteService.findRequeteById(requete);
		return Response.ok(retour).build();

	}

	@DELETE
	@Path("/{id}")
	public void deleteRequete(@PathParam("id") Integer id) {
		Requete requeteToDelete = new Requete();
		requeteToDelete.setIdRequete(id);
		requeteService.deleteRequete(requeteToDelete);
	}
	
	@POST
	public Response insertRequete(Requete requete){
		requete = requeteService.createRequete(requete);
		return Response.ok(requete).build();
	}
	
	@PUT
	@Path("/{id}")
	public Response updateRequete(Requete requete,@PathParam("id") Integer id){
		requete.setIdRequete(id);
		requete = requeteService.updateRequete(requete);
		return Response.ok(requete).build();
	}
}
