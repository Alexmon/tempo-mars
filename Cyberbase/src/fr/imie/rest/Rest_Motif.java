package fr.imie.rest;

import java.util.List;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import fr.cyberbase.model.Motif;
import fr.cyberbase.model.Usager;
import fr.cyberbase.model.Utilisation;
import fr.imie.service.IMotif;
import fr.imie.service.IUsagersService;

@Path("motif")
@Stateless
@Produces({ MediaType.APPLICATION_JSON })
@Consumes({ MediaType.APPLICATION_JSON })
public class Rest_Motif {
	
	@EJB
	private IMotif iMotifServ;
	@EJB
	private IUsagersService iUsagersServ;
	
	@GET
	public Response getAllMotif() {
		List<Motif> retour = iMotifServ.getAllMotif();
		return Response.ok(retour).build();

	}
	@GET
	@Path("/{id}")
	public Response getOne(@PathParam("id") Integer id){
		Motif mo = new Motif();
		mo.setIdMotif(id);
		Motif retour = iMotifServ.findUnMotifById(mo);
		return Response.ok(retour).build();

	}
	@GET
	@Path("/usagers/{id}")
	public Response getOnePersonnes(@PathParam("id") Integer id){
		Usager personne = new Usager();
		personne.setIdUsagers(id);
		Usager envoiePersonne = iUsagersServ.findUnUsagerById(personne);
		List<Utilisation> retour = iMotifServ.getAllMotifByUsagers(envoiePersonne);
		return Response.ok(retour).build();

	}
	@DELETE
	@Path("/{id}")
	public void deleteUtilisation(@PathParam("id") Integer id) {
		Motif MotifToDelete = new Motif();
		MotifToDelete.setIdMotif(id);
		iMotifServ.deleteUnMotif(MotifToDelete);
		
	}
	
	@POST
	public Response insertMotif(Motif mo){
		mo = iMotifServ.creerUnMotif(mo);
		return Response.ok(mo).build();
	}
	
	@PUT
	@Path("/{id}")
	public Response updateMotif(Motif mo,@PathParam("id") Integer id){
		mo.setIdMotif(id);
		mo = iMotifServ.updateUnMotif(mo);
		return Response.ok(mo).build();
	}
}
