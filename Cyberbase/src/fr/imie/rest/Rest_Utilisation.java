package fr.imie.rest;

import java.util.List;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import fr.cyberbase.model.Usager;
import fr.cyberbase.model.Utilisation;
import fr.imie.service.IUsagersService;
import fr.imie.service.IUtilisationServ;

@Path("utilisation")
@Stateless
@Produces({ MediaType.APPLICATION_JSON })
@Consumes({ MediaType.APPLICATION_JSON })
public class Rest_Utilisation {

				@EJB
				private IUtilisationServ iUtiliServ;
				@EJB
				private IUsagersService iUsagersServ;
	
		
		@GET
		public Response getAllPersonnes() {
			List<Utilisation> retour = iUtiliServ.getAllUtilisation();
			return Response.ok(retour).build();

		}
		@GET
		@Path("/usagers/{id}")
		public Response getOnePersonnes(@PathParam("id") Integer id){
			Usager personne = new Usager();
			personne.setIdUsagers(id);
			Usager envoiePersonne = iUsagersServ.findUnUsagerById(personne);
			System.out.println(envoiePersonne.getNomUsagers());
			List<Utilisation> retour = iUtiliServ.getAllUtilisationByUsagers(envoiePersonne);
			return Response.ok(retour).build();

		}
		@DELETE
		@Path("/{id}")
		public void deleteUtilisation(@PathParam("id") Integer id) {
			Utilisation utilisationToDelete = new Utilisation();
			utilisationToDelete.setIdUtilisation(id);;
			iUtiliServ.deleteUneUtilisation(utilisationToDelete);
			
		}
		
		@POST
		public Response insertUtilisation(Utilisation ut){
			ut = iUtiliServ.creerUneUtilisation(ut);
			return Response.ok(ut).build();
		}
		
}
