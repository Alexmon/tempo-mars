package fr.imie.rest;

import java.util.List;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import fr.cyberbase.model.Usager;
import fr.imie.service.IUsagersService;




@Path("usagers")
@Stateless
@Produces({ MediaType.APPLICATION_JSON })
@Consumes({ MediaType.APPLICATION_JSON })
public class Rest_Usagers {

	@EJB
	private IUsagersService usagerService;

	@GET
	public Response getAllPersonnes() {
		List<Usager> retour = usagerService.getListeUsagers();
		return Response.ok(retour).build();

	}
	
	@GET
	@Path("/{id}")
	public Response getOnePersonnes(@PathParam("id") Integer id){
		Usager personne = new Usager();
		personne.setIdUsagers(id);
		Usager retour = usagerService.findUnUsagerById(personne);
		return Response.ok(retour).build();

	}

	@DELETE
	@Path("/{id}")
	public void deletePersonne(@PathParam("id") Integer id) {
		Usager personneToDelete = new Usager();
		personneToDelete.setIdUsagers(id);
		usagerService.deleteUnUsager(personneToDelete);
	}
	
	@POST
	public Response insertPersonne(Usager personne){
		personne = usagerService.createUsagers(personne);
		return Response.ok(personne).build();
	}
	
	@PUT
	@Path("/{id}")
	public Response updatePersonne(Usager personne,@PathParam("id") Integer id){
		personne.setIdUsagers(id);
		personne = usagerService.updateUnUsager(personne);
		return Response.ok(personne).build();
	}
	
}
