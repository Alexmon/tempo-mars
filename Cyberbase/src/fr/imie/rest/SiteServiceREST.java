package fr.imie.rest;


import java.util.List;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import fr.cyberbase.model.Site;
import fr.cyberbase.model.SiteSalles;
import fr.imie.service.SiteServiceLocal;



@Path("site")
@Stateless
@Produces({ MediaType.APPLICATION_JSON })
@Consumes({ MediaType.APPLICATION_JSON })
public class SiteServiceREST {

	@EJB
	private SiteServiceLocal siteService;

	@GET
	public Response getAllSites() {
		List<Site> retour = siteService.findAll();
		return Response.ok(retour).build();

	}
	
	@GET
	@Path("/{id}")
	public Response getOneSites(@PathParam("id") Integer id){
		Site site = new Site();
		site.setIdSite(id);
		Site retour = siteService.findSiteById(site);
		return Response.ok(retour).build();

	}
	
	@GET
	@Path("/salles/{id}")
	public Response getSiteSalles(@PathParam("id") Integer id){
		SiteSalles site = new SiteSalles();
		site.setIdSite(id);
		SiteSalles retour = siteService.findSiteSallesById(site);
		return Response.ok(retour).build();

	}
	

	@DELETE
	@Path("/{id}")
	public void deleteSite(@PathParam("id") Integer id) {
		Site siteToDelete = new Site();
		siteToDelete.setIdSite(id);
		siteService.deleteSite(siteToDelete);
	}
	
	@POST
	public Response insertSite(Site site){
		site = siteService.createSite(site);
		return Response.ok(site).build();
	}
	
	@PUT
	@Path("/{id}")
	public Response updateSite(Site site,@PathParam("id") Integer id){
		site.setIdSite(id);
		site = siteService.updateSite(site);
		return Response.ok(site).build();
	}

}
