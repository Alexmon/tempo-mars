package fr.imie.rest;


import java.util.List;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import fr.cyberbase.model.Salle;
import fr.cyberbase.model.SallePostes;
import fr.imie.service.SalleServiceLocal;



@Path("salle")
@Stateless
@Produces({ MediaType.APPLICATION_JSON })
@Consumes({ MediaType.APPLICATION_JSON })
public class SalleServiceREST {

	@EJB
	private SalleServiceLocal salleService;

	@GET
	public Response getAllSalles() {
		List<Salle> retour = salleService.findAll();
		return Response.ok(retour).build();

	}
	
	@GET
	@Path("/{id}")
	public Response getOneSalles(@PathParam("id") Integer id){
		Salle salle = new Salle();
		salle.setIdSalle(id);
		Salle retour = salleService.findSalleById(salle);
		return Response.ok(retour).build();

	}
	
	@GET
	@Path("/postes/{id}")
	public Response getSallesPostes(@PathParam("id") Integer id){
		SallePostes salle = new SallePostes();
		salle.setIdSalle(id);
		SallePostes retour = salleService.findSallePostesById(salle);
		return Response.ok(retour).build();

	}
	
	@DELETE
	@Path("/{id}")
	public void deleteSalle(@PathParam("id") Integer id) {
		Salle salleToDelete = new Salle();
		salleToDelete.setIdSalle(id);
		salleService.deleteSalle(salleToDelete);
	}
	
	@POST
	public Response insertSalle(Salle salle){
		salle = salleService.createSalle(salle);
		return Response.ok(salle).build();
	}
	
	@PUT
	@Path("/{id}")
	public Response updateSalle(Salle salle,@PathParam("id") Integer id){
		salle.setIdSalle(id);
		salle = salleService.updateSalle(salle);
		return Response.ok(salle).build();
	}

}
