var listeutilisation = [];
var listeprovenance = [];
window.listeutilisation.push([ "Date_utilisation", "Heure_début", "Heure_fin",
		"Motif", "Poste", "Usager" ]);
window.listeprovenance.push([ "Nom", "Quartier" ]);

var listemotif = [];
var listeposte = [];
var listeusager = [];
var listequartier = [];

var sommemessagerie = 0;
var sommeformation = 0;
var sommecv = 0;
var sommerecherche = 0;

var sommeNantesEst = 0;
var sommeBellevue = 0;
var sommeChantenay = 0;
var sommeMalakoff = 0;
var sommeNantesNord = 0;
var sommeDervallieres = 0;
var sommeZola = 0;
var sommeHautsPaves = 0;
var sommeSaintFelix = 0;
var sommeNantesErdre = 0;
var sommeDoulon = 0;
var sommeBottiere = 0;
var sommeSaintDonatien = 0;
var sommeCentreville = 0;
var sommeNantesSud = 0;
var sommeIledeNantes = 0;
var sommeBreil = 0;
var sommeBarberie = 0;
var sommeRezePontRousseau = 0;
var sommeSaintHerblainSillon = 0;

$.ajax({
	url : 'api/motif',
	type : 'GET',
	datatype : 'json',
	crossDomain : true,
	async : false

}).done(function(data) {

	for (aa = 0; aa < data.length; aa++) {
		listemotif.push({
			label : data[aa].idMotif,
			value : data[aa].motif
		});

	}
});

$.ajax({
	url : 'api/poste',
	type : 'GET',
	datatype : 'json',
	crossDomain : true,
	async : false

}).done(function(data) {

	for (cc = 0; cc < data.length; cc++) {
		listeposte.push({
			label : data[cc].idPoste,
			value : data[cc].nomPoste
		});

	}
});

$.ajax({
	url : 'api/localisation',
	type : 'GET',
	datatype : 'json',
	crossDomain : true,
	async : false

}).done(function(data) {
	for (jj = 0; jj < data.length; jj++) {
		listequartier.push({
			label : data[jj].idQuartier,
			value : data[jj].nomQuartier
		});
	}
});

$.ajax({
	url : 'api/usagers',
	type : 'GET',
	datatype : 'json',
	crossDomain : true,
	async : false

}).done(
		function(data) {

			for (bb = 0; bb < data.length; bb++) {

				for (jj = 0; jj < listequartier.length; jj++) {

					if (listequartier[jj].label === data[bb].idQuartier) {

						listeusager.push({
							label : data[bb].idUsagers,
							value : data[bb].nomUsagers + ' '
									+ data[bb].prenomUsagers
						});

						window.listeprovenance.push([ listeusager[bb].label,
								listequartier[jj].label,
								listequartier[jj].value ]);
					}
				}
			}
		}).success(function(listeprovenance) {

	for (ff = 0; ff < listeprovenance.length; ff++) {

		if (listeprovenance[ff].idQuartier === 1) {
			window.sommeNantesEst++;
		}
		if (listeprovenance[ff].idQuartier === 2) {
			window.sommeBellevue++;
		}
		if (listeprovenance[ff].idQuartier === 3) {
			window.sommeChantenay++;
		}
		if (listeprovenance[ff].idQuartier === 4) {
			window.sommeMalakoff++;
		}
		if (listeprovenance[ff].idQuartier === 5) {
			window.sommeNantesNord++;
		}
		if (listeprovenance[ff].idQuartier === 6) {
			window.sommeDervallieres++;
		}
		if (listeprovenance[ff].idQuartier === 7) {
			window.sommeZola++;
		}
		if (listeprovenance[ff].idQuartier === 8) {
			window.sommeHautsPaves++;
		}
		if (listeprovenance[ff].idQuartier === 9) {
			window.sommeSaintFelix++;
		}
		if (listeprovenance[ff].idQuartier === 10) {
			window.sommeNantesErdre++;
		}
		if (listeprovenance[ff].idQuartier === 11) {
			window.sommeDoulon++;
		}
		if (listeprovenance[ff].idQuartier === 12) {
			window.sommeBottiere++;
		}
		if (listeprovenance[ff].idQuartier === 13) {
			window.sommeSaintDonatien++;
		}
		if (listeprovenance[ff].idQuartier === 14) {
			window.sommeCentreville++;
		}
		if (listeprovenance[ff].idQuartier === 15) {
			window.sommeNantesSud++;
		}
		if (listeprovenance[ff].idQuartier === 16) {
			window.sommeIledeNantes++;
		}
		if (listeprovenance[ff].idQuartier === 17) {
			window.sommeBreil++;
		}
		if (listeprovenance[ff].idQuartier === 18) {
			window.sommeBarberie++;
		}
		if (listeprovenance[ff].idQuartier === 19) {
			window.sommeRezePontRousseau++;
		}
		if (listeprovenance[ff].idQuartier === 20) {
			window.sommeSaintHerblainSillon++;
		}

	}
});

var janvier15 = 0;
var janvier14 = 0;
var janvier13 = 0;
var fevrier15 = 0;
var fevrier14 = 0;
var fevrier13 = 0;
var mars15 = 0;
var mars14 = 0;
var mars13 = 0;
var avril15 = 0;
var avril14 = 0;
var avril13 = 0;
var mai15 = 0;
var mai14 = 0;
var mai13 = 0;
var juin15 = 0;
var juin14 = 0;
var juin13 = 0;
var juillet15 = 0;
var juillet14 = 0;
var juillet13 = 0;
var aout15 = 0;
var aout14 = 0;
var aout13 = 0;
var septembre15 = 0;
var septembre14 = 0;
var septembre13 = 0;
var octobre15 = 0;
var octobre14 = 0;
var octobre13 = 0;
var novembre15 = 0;
var novembre14 = 0;
var novembre13 = 0;
var decembre15 = 0;
var decembre14 = 0;
var decembre13 = 0;

$
		.ajax({
			url : 'api/utilisation',
			type : 'GET',
			datatype : 'json',
			crossDomain : true,
			async : false

		})
		.done(
				function(data) {

					for (ii = 0; ii < data.length; ii++) {
						for (aa = 0; aa < listemotif.length; aa++) {
							if (listemotif[aa].label === data[ii].idMotif) {

								for (bb = 0; bb < listeusager.length; bb++) {
									if (listeusager[bb].label === data[ii].idUsagers) {

										for (cc = 0; cc < listeposte.length; cc++) {
											if (listeposte[cc].label === data[ii].idMotif) {

												for (ff = 0; ff < listeprovenance.length; ff++) {
													if (listeprovenance[ff].idUsagers === data[ii].idUsagers) {

														window.listeutilisation
																.push([
																		data[ii].dateUtilisation,
																		data[ii].heureDebUtilisation,
																		data[ii].heureFinUtilisation,
																		listemotif[aa].value,
																		listeposte[cc].value,
																		listeusager[bb].label,
																		listeprovenance[ff].idQuartier,
																		listeprovenance[ff].value ]);

													}
												}
											}
										}
									}
								}
							}
						}
					}
				})
		.success(
				function(listeutilisation) {

					for (dd = 0; dd < listeutilisation.length; dd++) {

						if (listeutilisation[dd].idMotif === 1) {
							window.sommecv++;

						}

						if (listeutilisation[dd].idMotif === 2) {
							window.sommerecherche++;

						}

						if (listeutilisation[dd].idMotif === 3) {
							window.sommeformation++;

						}

						if (listeutilisation[dd].idMotif === 4) {
							window.sommemessagerie++;

						}
					}

					var pattern2016 = new RegExp(
							"20{1}[0-9]{1}[6]{1}[-][0-1]{1}[0-9]{1}[-][0-3]{1}[0-9]{1}");
					var pattern2015 = new RegExp(
							"20{1}[0-9]{1}[5]{1}[-][0-1]{1}[0-9]{1}[-][0-3]{1}[0-9]{1}");
					var pattern2014 = new RegExp(
							"20{1}[0-9]{1}[4]{1}[-][0-1]{1}[0-9]{1}[-][0-3]{1}[0-9]{1}");
					var pattern2013 = new RegExp(
							"20{1}[0-9]{1}[3]{1}[-][0-1]{1}[0-9]{1}[-][0-3]{1}[0-9]{1}");
					var pattern2012 = new RegExp(
							"20{1}[0-9]{1}[2]{1}[-][0-1]{1}[0-9]{1}[-][0-3]{1}[0-9]{1}");

					var pattern01 = new RegExp(
							"20{1}[0-9]{2}[-]{1}[0]{1}[1]{1}[-][0-3]{1}[0-9]{1}");
					var pattern02 = new RegExp(
							"20{1}[0-9]{2}[-]{1}[0]{1}[2]{1}[-][0-3]{1}[0-9]{1}");
					var pattern03 = new RegExp(
							"20{1}[0-9]{2}[-]{1}[0]{1}[3]{1}[-][0-3]{1}[0-9]{1}");
					var pattern04 = new RegExp(
							"20{1}[0-9]{2}[-]{1}[0]{1}[4]{1}[-][0-3]{1}[0-9]{1}");
					var pattern05 = new RegExp(
							"20{1}[0-9]{2}[-]{1}[0]{1}[5]{1}[-][0-3]{1}[0-9]{1}");
					var pattern06 = new RegExp(
							"20{1}[0-9]{2}[-]{1}[0]{1}[6]{1}[-][0-3]{1}[0-9]{1}");
					var pattern07 = new RegExp(
							"20{1}[0-9]{2}[-]{1}[0]{1}[7]{1}[-][0-3]{1}[0-9]{1}");
					var pattern08 = new RegExp(
							"20{1}[0-9]{2}[-]{1}[0]{1}[8]{1}[-][0-3]{1}[0-9]{1}");
					var pattern09 = new RegExp(
							"20{1}[0-9]{2}[-]{1}[0]{1}[9]{1}[-][0-3]{1}[0-9]{1}");
					var pattern10 = new RegExp(
							"20{1}[0-9]{2}[-]{1}[1]{1}[0]{1}[-][0-3]{1}[0-9]{1}");
					var pattern11 = new RegExp(
							"20{1}[0-9]{2}[-]{1}[1]{1}[1]{1}[-][0-3]{1}[0-9]{1}");
					var pattern12 = new RegExp(
							"20{1}[0-9]{2}[-]{1}[1]{1}[2]{1}[-][0-3]{1}[0-9]{1}");

				
					
					for (dd = 0; dd < listeutilisation.length; dd++) {

						var controle2016 = pattern2016
								.test(listeutilisation[dd].dateUtilisation);
						var controle2015 = pattern2015
								.test(listeutilisation[dd].dateUtilisation);
						var controle2014 = pattern2014
								.test(listeutilisation[dd].dateUtilisation);
						var controle2013 = pattern2013
								.test(listeutilisation[dd].dateUtilisation);
						var controle2012 = pattern2012
								.test(listeutilisation[dd].dateUtilisation);

						if (controle2015 === true) {
							var controle01 = pattern01
									.test(listeutilisation[dd].dateUtilisation);
							var controle02 = pattern02
									.test(listeutilisation[dd].dateUtilisation);
							var controle03 = pattern03
									.test(listeutilisation[dd].dateUtilisation);
							var controle04 = pattern04
									.test(listeutilisation[dd].dateUtilisation);
							var controle05 = pattern05
									.test(listeutilisation[dd].dateUtilisation);
							var controle06 = pattern06
									.test(listeutilisation[dd].dateUtilisation);
							var controle07 = pattern07
									.test(listeutilisation[dd].dateUtilisation);
							var controle08 = pattern08
									.test(listeutilisation[dd].dateUtilisation);
							var controle09 = pattern09
									.test(listeutilisation[dd].dateUtilisation);
							var controle10 = pattern10
									.test(listeutilisation[dd].dateUtilisation);
							var controle11 = pattern11
									.test(listeutilisation[dd].dateUtilisation);
							var controle12 = pattern12
									.test(listeutilisation[dd].dateUtilisation);

							if (controle01 === true) {

								window.janvier15++;
							
							}

							if (controle02 === true) {

								window.fevrier15++;
							}

							if (controle03 === true) {

								window.mars15++;
							}

							if (controle04 === true) {

								window.avril15++;
							}

							if (controle05 === true) {

								window.mai15++;
							}

							if (controle06 === true) {

								window.juin15++;
							}

							if (controle07 === true) {

								window.juillet15++;
							}

							if (controle08 === true) {

								window.aout15++;
							}

							if (controle09 === true) {

								window.septembre15++;
							}

							if (controle10 === true) {

								window.octobre15++;
								
							}

							if (controle11 === true) {

								window.novembre15++;
							}

							if (controle12 === true) {

								window.decembre15++;
							}

						}
						if (controle2014 === true) {
							var controle01 = pattern01
									.test(listeutilisation[dd].dateUtilisation);
							var controle02 = pattern02
									.test(listeutilisation[dd].dateUtilisation);
							var controle03 = pattern03
									.test(listeutilisation[dd].dateUtilisation);
							var controle04 = pattern04
									.test(listeutilisation[dd].dateUtilisation);
							var controle05 = pattern05
									.test(listeutilisation[dd].dateUtilisation);
							var controle06 = pattern06
									.test(listeutilisation[dd].dateUtilisation);
							var controle07 = pattern07
									.test(listeutilisation[dd].dateUtilisation);
							var controle08 = pattern08
									.test(listeutilisation[dd].dateUtilisation);
							var controle09 = pattern09
									.test(listeutilisation[dd].dateUtilisation);
							var controle10 = pattern10
									.test(listeutilisation[dd].dateUtilisation);
							var controle11 = pattern11
									.test(listeutilisation[dd].dateUtilisation);
							var controle12 = pattern12
									.test(listeutilisation[dd].dateUtilisation);

							if (controle01 === true) {
								window.janvier14++;

							}

							if (controle02 === true) {
								window.fevrier14++;
							}

							if (controle03 === true) {
								window.mars14++;
							}

							if (controle04 === true) {
								window.avril14++;
							}

							if (controle05 === true) {
								window.mai14++;
							}

							if (controle06 === true) {
								window.juin14++;
							}

							if (controle07 === true) {
								window.juillet14++;
							}

							if (controle08 === true) {
								window.aout14++;
							}

							if (controle09 === true) {
								window.septembre14++;
							}

							if (controle10 === true) {

								window.octobre14++;
							}

							if (controle11 === true) {
								window.novembre14++;
							}

							if (controle12 === true) {
								window.decembre14++;
							}

						}

						if (controle2013 === true) {
							var controle01 = pattern01
									.test(listeutilisation[dd].dateUtilisation);
							var controle02 = pattern02
									.test(listeutilisation[dd].dateUtilisation);
							var controle03 = pattern03
									.test(listeutilisation[dd].dateUtilisation);
							var controle04 = pattern04
									.test(listeutilisation[dd].dateUtilisation);
							var controle05 = pattern05
									.test(listeutilisation[dd].dateUtilisation);
							var controle06 = pattern06
									.test(listeutilisation[dd].dateUtilisation);
							var controle07 = pattern07
									.test(listeutilisation[dd].dateUtilisation);
							var controle08 = pattern08
									.test(listeutilisation[dd].dateUtilisation);
							var controle09 = pattern09
									.test(listeutilisation[dd].dateUtilisation);
							var controle10 = pattern10
									.test(listeutilisation[dd].dateUtilisation);
							var controle11 = pattern11
									.test(listeutilisation[dd].dateUtilisation);
							var controle12 = pattern12
									.test(listeutilisation[dd].dateUtilisation);

							if (controle01 === true) {
								
								window.janvier13++;

							}

							if (controle02 === true) {

								window.fevrier13++;
							}

							if (controle03 === true) {

								window.mars13++;
							}

							if (controle04 === true) {

								window.avril13++;
							}

							if (controle05 === true) {

								window.mai13++;
							}

							if (controle06 === true) {

								window.juin13++;
							}

							if (controle07 === true) {
	
								window.juillet13++;
							}

							if (controle08 === true) {

								window.aout13++;
							}

							if (controle09 === true) {

								window.septembre13++;
							}

							if (controle10 === true) {

								window.octobre13++;
								
							}

							if (controle11 === true) {

								window.novembre13++;
							}

							if (controle12 === true) {
	
								window.decembre13++;
							}

						}

					}

				});



var barChartData = {
	labels : [ "Nantes Est", "Bellevue", "Chantenay", "Malakoff",
			"Nantes Nord", "Dervalli\350res", "Zola", "Hauts Pav\351s",
			"Saint Felix", "Nantes Erdre", "Doulon", "Botti\350re",
			"Saint Donatien", "Centre ville", "Nantes Sud", "Ile de Nantes",
			"Breil", "Barberie", "Rez\351 Pont-Rousseau", "Saint Herblain Sillon" ],
	datasets : [ {
		label : "Cumul des visites par site",
		fillColor : "rgba(73,149,182,0.5)",
		strokeColor : "rgba(73,149,182,0.8)",
		highlightFill : "rgba(73,149,182,0.75)",
		highlightStroke : "rgba(73,149,182,1)",
		data : [ sommeNantesEst, sommeBellevue, sommeChantenay, sommeMalakoff,
				sommeNantesNord, sommeDervallieres, sommeZola, sommeHautsPaves,
				sommeSaintFelix, sommeNantesErdre, sommeDoulon, sommeBottiere,
				sommeSaintDonatien, sommeCentreville, sommeNantesSud,
				sommeIledeNantes, sommeBreil, sommeBarberie,
				sommeRezePontRousseau, sommeSaintHerblainSillon, ]
	} ]

}



var data = {
	labels : [ "janvier", "février", "mars", "avril", "mai", "juin", "juillet",
			"août", "septembre", "octobre", "novembre", "decembre" ],
	datasets : [
			{
				label : "Cumul des connexions pour l'ann\351e 2015",
				fillColor : "rgba(73,149,182,0.5)",
				strokeColor : "rgba(73,149,182,1)",
				pointColor : "rgba(73,149,182,1)",
				pointStrokeColor : "#fff",
				pointHighlightFill : "#fff",
				pointHighlightStroke : "rgba(73,149,182,1)",
				data : [ janvier15, fevrier15, mars15, avril15, mai15, juin15,
						juillet15, aout15, septembre15, octobre15, novembre15,
						decembre15 ]
			},
			{
				label : "Cumul des connexions pour l'ann\351e 2014",
				fillColor : "rgba(247,146,86,0.2)",
				strokeColor : "rgba(247,146,86,1)",
				pointColor : "rgba(247,146,86,1)",
				pointStrokeColor : "#fff",
				pointHighlightFill : "#fff",
				pointHighlightStroke : "rgba(247,146,86,1)",
				data : [ janvier14, fevrier14, mars14, avril14, mai14, juin14,
						juillet14, aout14, septembre14, octobre14, novembre14,
						decembre14 ]
			},
			{
				label : "Cumul des connexions pour l'ann\351e 2013",
				fillColor : "rgba(129,182,205,0.2)",
				strokeColor : "rgba(129,182,205,1)",
				pointColor : "rgba(129,182,205,1)",
				pointStrokeColor : "#fff",
				pointHighlightFill : "#fff",
				pointHighlightStroke : "rgba(129,182,205,1)",
				data : [ janvier13, fevrier13, mars13, avril13, mai13, juin13,
						juillet13, aout13, septembre13, octobre13, novembre13,
						decembre13 ]
			} ]
};

var doughnutData = [ {
	value : sommeformation,
	color : "#F7464A",
	highlight : "#FF5A5E",
	label : "Formation"
}, {
	value : sommecv,
	color : "#46BFBD",
	highlight : "#5AD3D1",
	label : "CV"
}, {
	value : sommerecherche,
	color : "#4E0F6D",
	highlight : "#5A1F6D",
	label : "Recherche d'emploi"
}, {
	value : sommemessagerie,
	color : "#FFB500",
	highlight : "#FFB501",
	label : "Messagerie"
}

];



var fonction1 = function() {
	var ctx = document.getElementById("motif").getContext("2d");
	window.myDoughnut = new Chart(ctx).Doughnut(doughnutData, {
		responsive : true
	});
}

var fonction2 = function() {
	var ctx = document.getElementById("provenance").getContext("2d");
	window.myBar = new Chart(ctx).Bar(barChartData, {
		responsive : true}
	);
};

var fonction3 = function() {
	var ctx = document.getElementById("annees").getContext("2d");
	var myLineChart = new Chart(ctx).Line(data,	{responsive : true});
	legend(document.getElementById("Legendaire"), data);

};

window.onload = function() {
	fonction1();
	fonction2();
	fonction3();

};
