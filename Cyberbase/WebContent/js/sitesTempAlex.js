$(document).ready(function() { 
    // Fonction Listener    
 
	//La fonction ListSites affiche le listing des sites dans une div site au chargement de la page
$('#edit').ready(function ListSites () {
  
    $.ajax({
      url: 'http://10.0.10.138:8080/Cyberbase/api/site/',
    //url: 'json/sites.json',
        
      type: 'GET',
      dataType: 'json',
        /*Chrome n'accepte pas en local*/
       crossDomain :true
    }).done(function (data) {
	
	 var sites ="";
        for (ii=0; ii<data.length; ii++) {
            
            
		sites += '<div id="siteok">'
        + '<span class="clickSites" id='+ data[ii].idSite +'><img src="img/responsive-mobile-icon-set/cold/png/64/gps.png"></span>'+ "<br>"
		+ '<span class="libelSites">Site :' + "<br>" + data[ii].nomSite + '</span>' + " " + "<br>"
        + '<span >CP : ' +" "+ data[ii].cpSite + '</span>'+" " + "<br>"
        + '</div>' 
        
        };
	$("#site").html(sites);
      
    });
  
  });
// La fonction SalleSite permet au clic sur un site d'afficher les salles correspondantes dans une div showSalles
    $("#site").on('click', ".clickSites", function SalleSite(){
        var clickid = this.id;
        
        $('#editSalles').ready(function () {
        $.ajax({
      url: 'http://10.0.10.138:8080/Cyberbase/api/site/salles/'+ clickid,
      //url: 'json/salles.json',
        type: 'GET',
        dataType: 'json',
        crossDomain :true
        	}).done (function (data){
        
            var salles ="";
            
           for (ii=0; ii<data.salle.length; ii++) {
            salles+='<div id="salleok">'
		+'<span class="clickSalles" id='+data.salle[ii].idSalle+'><img src="img/apps.png"></span>'+"<br>"
        + '<span class="libelSalles">'+data.salle[ii].nomSalle+ '</span>'+"<br>"
        + '</div>'
           		};
           		//Affiche les infos correspondantes au site sélectionné
           		salles+='<div id="infoSites">'+"<br>"+'<span class="MemoSite">'+"Coordonnees du site de "+data.salle[0].site.nomSite+'</span>'+"<br>"+"<br>"
           		+'<span class="AdrSite">'+"Adresse : "+data.salle[0].site.adresseSite+'</span>'+"<br>"
           		+'<span class="CpSite">'+data.salle[0].site.cpSite+" "+data.salle[0].site.villeSite+'</span>'+"<br>"
           		+'<span class="TelSite">'+"Telephone : "+data.salle[0].site.telephoneSite+'</span>'+"<br>"
           		+'</div>';
            $("#showSalles").html(salles);
            
            });
        });
    });
});