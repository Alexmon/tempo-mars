(function () {
	
	
	////////////////::GESTION DES BOUTONS:://////////////
	$('#sites, #sitesRight').click(function(){
		$('#secCentr').hide();
		$(pageNormale()).ready(function(){
			chargerLesSites();	
		});
	});
	
	$('#usagers, #usagersRight').click(function(){
		$('#secCentr').hide();
		$(pageNormale()).ready(function(){
			chargerLesPostes();	
		});
		});

	
	
	////////////////:: FIN GESTION DES BOUTONS:://////////////
	
	////////////////:: CREATION DU SQUELETTE NORMAL D'UNE PAGE///////////:
	function pageNormale(){
		$('#secCentr').html(' <article class="artLeftSites"><div id="edit"></div><div id="site"></div></article><article id="secCentrMaps"><div id="editSalles"></div><div id="showSalles"></div>   </article><article id="secRightpages"><div class="boutonRight" id="sitesRight">Sites</div><div class="boutonRight" id="usagersRight">Usagers</div><div class="boutonRight" id="statistiquesRight">Statistiques</div></article>');
		$('#secCentr').show();
	}
////////////////:: FIN CREATION DU SQUELETTE NORMAL D'UNE PAGE///////////:
	
////////////////:: CREATION DE LA PAGE DES SITES:::///////////:
	function chargerLesSites(){
		$('#edit').ready(function ListSites () {
  
    $.ajax({
      url: 'http://10.0.10.138:8080/Cyberbase/api/site/',
    //url: 'json/sites.json',
        
      type: 'GET',
      dataType: 'json',
        /*Chrome n'accepte pas en local*/
       crossDomain :true
    }).done(function (data) {
	
	 var sites ="";
        for (ii=0; ii<data.length; ii++) {
            
            
		sites += '<div id="siteok">'
        + '<span class="clickSites" id='+ data[ii].idSite +'><img src="img/responsive-mobile-icon-set/cold/png/64/gps.png"></span>'+ "<br>"
		+ '<span class="libelSites">Site :' + "<br>" + data[ii].nomSite + '</span>' + " " + "<br>"
        + '<span >CP : ' +" "+ data[ii].cpSite + '</span>'+" " + "<br>"
        + '</div>' 
        
        };
	$("#site").html(sites);
      
    });
  
  });
// La fonction SalleSite permet au clic sur un site d'afficher les salles correspondantes dans une div showSalles
    $("#site").on('click', ".clickSites", function SalleSite(){
        var clickid = this.id;
        
        $('#editSalles').ready(function () {
        $.ajax({
      url: 'http://10.0.10.138:8080/Cyberbase/api/site/salles/'+ clickid,
      //url: 'json/salles.json',
        type: 'GET',
        dataType: 'json',
        crossDomain :true
        	}).done (function (data){
        
            var salles ="";
            
           for (ii=0; ii<data.salle.length; ii++) {
            salles+='<div id="salleok">'
		+'<span class="clickSalles" id='+data.salle[ii].idSalle+'><img src="img/apps.png"></span>'+"<br>"
        + '<span class="libelSalles">'+data.salle[ii].nomSalle+ '</span>'+"<br>"
        + '</div>'
           		};
           		//Affiche les infos correspondantes au site sélectionné
           		salles+='<div id="infoSites">'+"<br>"+'<span class="MemoSite">'+"Coordonnees du site de "+data.salle[0].site.nomSite+'</span>'+"<br>"+"<br>"
           		+'<span class="AdrSite">'+"Adresse : "+data.salle[0].site.adresseSite+'</span>'+"<br>"
           		+'<span class="CpSite">'+data.salle[0].site.cpSite+" "+data.salle[0].site.villeSite+'</span>'+"<br>"
           		+'<span class="TelSite">'+"Telephone : "+data.salle[0].site.telephoneSite+'</span>'+"<br>"
           		+'</div>';
            $("#showSalles").html(salles);
            
            });
        });
    });
	
	};
////////////////:: FIN CREATION DE LA PAGE DES SITES:::///////////:
	function chargerLesPostes(){
	var tabDone = false;

	  $('#edit').ready(function () {
		  
		  $( "#planningPoste" ).hide();
		  $( "#secCentr" ).hide();
		  
		  
		  
	    $.ajax({
	      url: 'http://10.0.10.138:8080/Cyberbase/api/poste',
	      type: 'GET',
	      dataType: 'json',
	        /*Chrome n'accepte pas en local*/
	        /*crossDomain :true*/
	    }).done(function (data) {

	        
		 var postes ="";

	    
	    for (ii=0; ii<data.length; ii++) {
	    	postes += '<div id="posteok">';
	            if (data[ii].salle.idSalle === 1 && data[ii].visiblePoste === true) {
	            
			
	            // (data[ii].visiblePoste === true)
			
	            	postes += '<p id='+data[ii].idPoste + ' data-name ="'+ data[ii].nomPoste +'"><img src="img/desktop.png">'+ "<br>"
	            	+ '<span class="libel">Poste id' +" "+data[ii].idPoste + '</span>'+" "+ "<br>"
	            	+ '<span class="libel">Nom poste'+" "+ data[ii].nomPoste + '</span>'+" " + "<br>"
	            	+ '<span class="libel">Salle' +" "+ data[ii].salle.idSalle + '</span>'+" " + "<br>" 
	            	+ "</p>";
	        
	    
	            
	            } else {
	            	
	            console.log('erreur : poste hors de la salle');
	            
	            }
	            postes +=  "</div>";
	        };
	 
			
		$("#poste").html(postes);
		$( "p" ).click( function(){
			var PosteId =null;
			var PosteNom = null;
			 $( "#planningPoste" ).hide();
			  $( "#secCentr" ).hide();

			var tabRef = ["08:00:00","09:00:00","10:00:00","11:00:00","12:00:00","13:00:00","14:00:00","15:00:00","16:00:00","17:00:00","18:00:00"];
	    	var tableHeure ="";
			var PosteId = this.id;
			var PosteNom = this.dataset.name;
			console.log(PosteNom);
	    	$.ajax({
			      url: 'http://10.0.10.138:8080/Cyberbase/api/poste/today/'+this.id,
			      type: 'GET',
			      dataType: 'json',
			        /*Chrome n'accepte pas en local*/
			        /*crossDomain :true*/
			    }).done(function (data) {
			    	tableHeure +="<table><tr><th>heure</th><th>disponibilité</th></tr>";
			    for(jj = 0 ; jj < tabRef.length ; jj++){
			    	var trouve = false;
			    	tableHeure += '<tr><td><span class="tableTime">'+tabRef[jj]+'</span></td>';
			    	for (ii=0; ii<data.length; ii++) {
			    		if(tabRef[jj] >= data[ii].heureDebUtilisation && tabRef[jj] < data[ii].heureFinUtilisation){
			    			trouve = true;
			    		}
			    		if(trouve){
			    		break;
			    		}
			    	}
			    	if(trouve){
			    		tableHeure+='<td class="tableTimeOQP" id="'+data[ii]+'"> RDV </td></tr>';
			    	}else{
			    		tableHeure+='<td class="tableTimeLibre" id="'+PosteId +'" lang ="'+ tabRef[jj] +'"> ---- </td></tr>';
			    	}
			    }
			    tableHeure +="</table>";
			    $( "#planning" ).html(tableHeure);
			    tabDone = true;
			    showTab();

			    function showTab(){
			  	  if(tabDone){
			  			$( "#planningPoste" ).show();
				  		}
			  	  $('.tableTimeLibre').click(function(){

			  			console.log('Creneau libre pour le poste :'+this.id + 'a :' +this.lang);
			  			$( '#titreInfo').append(' '+PosteNom + '<br> pour le creneau de ' +this.lang )
			  			$( '#sujet').append(motif());
			  			$( "#secCentr" ).show();
			  		})

			    }
			    function motif(){
			    $.ajax({
				      url: 'api/motif/',
				      type: 'GET',
				      dataType: 'json',
				        /*Chrome n'accepte pas en local*/
				        /*crossDomain :true*/
				    }).done(function (data) {
				    	for(item in data){
				    		$('#sujet').append('<option value='+data[item].idMotif+'>'+data[item].motif+'</option>');
				    	}
				    });
			    };
		  });
		  })
	     });
	   });
	};
////////////////:: CREATION DE LA PAGE DES POSTES:::///////////:	

	
		  }).call(this);