function legend(parent, data) {
parent.className = 'legend';
var datas = data.hasOwnProperty('datasets') ? data.datasets : data;
// remove possible children of the parent
while(parent.hasChildNodes()) {
parent.removeChild(parent.lastChild);
}
datas.forEach(function(d) {
	var width = 12;
	var height = 12;


	
var title = document.createElement('div');
title.className = 'title';

parent.appendChild(title);
var colorSample = document.createElement('p');
colorSample.className = 'color-sample';
colorSample.style.border = d.hasOwnProperty('strokeColor') ? d.strokeColor : d.color;
colorSample.style.borderStyle = "solid";
colorSample.style.borderWidth = "2px";
colorSample.style.backgroundColor = d.hasOwnProperty('fillColor') ? d.fillColor : d.color;
colorSample.style.width = width + "px";
colorSample.style.height = height + "px";
title.appendChild(colorSample);

var label = document.createElement('span');
var text = document.createTextNode(d.label);
label.className='label';
text.className='text';

title.appendChild(label);
label.appendChild(text);

});
}
