

	/*----- Script d'autocomplétion sur les noms d'usagers-----*/
$(document).ready(function(){
var v="";
var listenom = [];
var listequartier = [];
var listecsp = [];
var listeformation = [];
$("#formulaire" ).hide();

function refresh(){

	if ('idUsagers' in sessionStorage){
		$.ajax({
			url : 'api/usagers',
			type : 'GET',
			datatype : 'json',
			crossDomain : true 

		}).done(function(data) {
		for (ii=0; ii<data.length; ii++){
			  if (sessionStorage.getItem('idUsagers') == data[ii].idUsagers) {

					 //Remplissage des champs de consultation de la fiche usager
			  $("#formulaire" ).show();
			  $( "#civilite" ).val( data[ii].civiliteUsagers);
			  $( "#prenom" ).val( data[ii].prenomUsagers   );
			  $( "#nom" ).val(data[ii].nomUsagers );
			  $( "#dateNaiss" ).val( data[ii].dateNaissUsagers );
			  $( "#cp" ).val( data[ii].cpUsagers );
			  $( "#mail" ).val( data[ii].mailUsagers );
			  $( "#checkExclu" ).prop('checked', data[ii].exclusion);
			  
			  for (jj=0; jj <listequartier.length; jj++){ 
				  
					if (listequartier[jj].label === data[ii].idQuartier){

						$( "#quartier" ).val( listequartier[jj].value);}
				 };
			  for (kk=0; kk <listecsp.length; kk++){ 
						if (listecsp[kk].label === data[ii].idCsp){
							$( "#csp" ).val( listecsp[kk].value);}
					 };
			  for (aa=0; aa <listeformation.length; aa++){ 
							if (listeformation[aa].label === data[ii].idNivFormation){
								$( "#formation" ).val( listeformation[aa].value);}
						 };
			  
			  $( "#checkMDE" ).prop('checked', data[ii].suiviMissionLocale);


			  //Remplissage des champs de la modale de modification de la fiche usager
			  $("#id_cache").val(data[ii].id);
			  $( "#Mod_civilite" ).val( data[ii].civiliteUsagers);
			  $( "#Mod_prenom" ).val( data[ii].prenomUsagers   );
			  $( "#Mod_nom" ).val(data[ii].nomUsagers );
			  $( "#Mod_dateNaiss" ).val( data[ii].dateNaissUsagers );
			  $( "#Mod_cp" ).val( data[ii].cpUsagers );
			  $( "#Mod_mail" ).val( data[ii].mailUsagers );
			  $( "#Mod_checkExclu" ).prop('checked', data[ii].exclusion);

			  var quartiers ="";
			  
			  for (jj=0; jj <listequartier.length; jj++){ 

				  quartiers += '<option value="'
					  + listequartier[jj].label
					  + '"'
					  if (listequartier[jj].label === data[ii].idQuartier){
							quartiers += ' selected'  }
				  quartiers +='>'
					  + listequartier[jj].value
					  + '</option>'
							
				};
				$('#Mod_quartier').append( quartiers);

				var csps="";
			  for (kk=0; kk <listecsp.length; kk++){ 

				  csps += '<option value="'
					  + listecsp[kk].label
					  + '"'
						if (listecsp[kk].label === data[ii].idCsp){
							csps += ' selected' }
					csps +='>'
						  + listecsp[kk].value
						  + '</option>'
										 };
										 $('#Mod_csp').append( csps);	
						var formations ="";				 				 
			  for (aa=0; aa <listeformation.length; aa++){ 
				  formations += '<option value="'
					  + listeformation[aa].label
					  + '"'
							if (listeformation[aa].label === data[ii].idNivFormation){
								formations += ' selected'}
				  formations +='>'
					  + listeformation[aa].value
					  + '</option>'
						 };
			 
						 $('#Mod_formation').append( formations);	
				
			  
			  $( "#Mod_checkMDE" ).prop('checked', data[ii].suiviMissionLocale);
			  $( "#Mod_visible" ).prop('checked', data[ii].visible);
			  
			  $("#modifier").css("display","block");
			  var id = data[ii].idUsagers;
			  $("#id_cache").attr("data-id", data[ii].idUsagers);
			  sessionStorage.clear();
		  			  

				} 
				}
			})
		}

};

		
		//récupération de la source JSON (fichier local ou distant)	
					// $.getJSON( "usager.json", function( data ) {
						
						 $.ajax({
											url : 'api/localisation',
											type : 'GET',
											datatype : 'json',
											crossDomain : true 

										}).done(function (data) {
											for (ii=0; ii<data.length; ii++){
								listequartier.push({label:data[ii].idQuartier, value:data[ii].nomQuartier});
							    												}});
						
						 $.ajax({
								url : 'api/csp',
								type : 'GET',
								datatype : 'json',
								crossDomain : true 

							}).done(function (data) {
								for (ii=0; ii<data.length; ii++){
					listecsp.push({label:data[ii].idCsp, value:data[ii].csp});
				    												}});

						 $.ajax({
								url : 'api/nivformation',
								type : 'GET',
								datatype : 'json',
								crossDomain : true 

							}).done(function (data) {
								for (ii=0; ii<data.length; ii++){
					listeformation.push({label:data[ii].idNivFormation, value:data[ii].libNivFormation});
				    												}});
						$('#recherche').show(function() {
						 $.ajax({

											url : 'api/usagers',
											type : 'GET',
											datatype : 'json',
											crossDomain : true 

										}).done(function(data) {

											//création de la source du champ d'autocomplétion (label et value)
				
				
				for (ii=0; ii<data.length; ii++){
					listenom.push({label:data[ii].nomUsagers +' '+ data[ii].prenomUsagers, value:data[ii].idUsagers });
					}
				refresh();			
				//utilisation du plugin jquery d'autocomplétion 
				 $("#formulaire" ).hide();
				 
$("#recherche").autocomplete({source :listenom, minLength : 2, select: function (event, ui) {
	//récupération des label/value
    var l = ui.item.label;
    var v= ui.item.value;
//    affectation de la valeur idUsager en sessionStorage
    sessionStorage.setItem('idUsagersKey', v);
    sessionStorage.setItem("nomUsagerKey", l);
    // mise à jour de la valeur affichée
    $('#recherche').html(l);
    this.value = l; 

	

    //remplissage automatique des champs 
   for (ii=0; ii<data.length; ii++){
		  if (v === data[ii].idUsagers) {
				if (data[ii].visibleUsagers === true){

					 //Remplissage des champs de consultation de la fiche usager
			  $("#formulaire" ).show();
			  $( "#civilite" ).val( data[ii].civiliteUsagers);
			  $( "#prenom" ).val( data[ii].prenomUsagers   );
			  $( "#nom" ).val(data[ii].nomUsagers );
			  $( "#dateNaiss" ).val( data[ii].dateNaissUsagers );
			  $( "#cp" ).val( data[ii].cpUsagers );
			  $( "#mail" ).val( data[ii].mailUsagers );
			  $( "#checkExclu" ).prop('checked', data[ii].exclusion);
			  
			  for (jj=0; jj <listequartier.length; jj++){ 
				  
					if (listequartier[jj].label === data[ii].idQuartier){

						$( "#quartier" ).val( listequartier[jj].value);}
				 };
			  for (kk=0; kk <listecsp.length; kk++){ 
						if (listecsp[kk].label === data[ii].idCsp){
							$( "#csp" ).val( listecsp[kk].value);}
					 };
			  for (aa=0; aa <listeformation.length; aa++){ 
							if (listeformation[aa].label === data[ii].idNivFormation){
								$( "#formation" ).val( listeformation[aa].value);}
						 };
			  
			  $( "#checkMDE" ).prop('checked', data[ii].suiviMissionLocale);


			  //Remplissage des champs de la modale de modification de la fiche usager
			  $("#id_cache").val(data[ii].id);
			  $( "#Mod_civilite" ).val( data[ii].civiliteUsagers);
			  $( "#Mod_prenom" ).val( data[ii].prenomUsagers   );
			  $( "#Mod_nom" ).val(data[ii].nomUsagers );
			  $( "#Mod_dateNaiss" ).val( data[ii].dateNaissUsagers );
			  $( "#Mod_cp" ).val( data[ii].cpUsagers );
			  $( "#Mod_mail" ).val( data[ii].mailUsagers );
			  $( "#Mod_checkExclu" ).prop('checked', data[ii].exclusion);

			  var quartiers ="";
			  
			  for (jj=0; jj <listequartier.length; jj++){ 

				  quartiers += '<option value="'
					  + listequartier[jj].label
					  + '"'
					  if (listequartier[jj].label === data[ii].idQuartier){
							quartiers += ' selected'  }
				  quartiers +='>'
					  + listequartier[jj].value
					  + '</option>'
							
				};
				$('#Mod_quartier').append( quartiers);

				var csps="";
			  for (kk=0; kk <listecsp.length; kk++){ 

				  csps += '<option value="'
					  + listecsp[kk].label
					  + '"'
						if (listecsp[kk].label === data[ii].idCsp){
							csps += ' selected' }
					csps +='>'
						  + listecsp[kk].value
						  + '</option>'
										 };
										 $('#Mod_csp').append( csps);	
						var formations ="";				 				 
			  for (aa=0; aa <listeformation.length; aa++){ 
				  formations += '<option value="'
					  + listeformation[aa].label
					  + '"'
							if (listeformation[aa].label === data[ii].idNivFormation){
								formations += ' selected'}
				  formations +='>'
					  + listeformation[aa].value
					  + '</option>'
						 };
			 
						 $('#Mod_formation').append( formations);	
				
			  
			  $( "#Mod_checkMDE" ).prop('checked', data[ii].suiviMissionLocale);
			  $( "#Mod_visible" ).prop('checked', data[ii].visible);
			  
			  $("#modifier").css("display","block");
			  var id = data[ii].idUsagers;
			  $("#id_cache").attr("data-id", data[ii].idUsagers);
			  			  			  

				} 

				else{ $("#visible" ).css("display","block");			 }
				
						  }}
 //désactivation de l'affichage de la value
   return false; 
	  }});

//création de la modale

	$('a.poplight').on('click', function() {
		var popID = $(this).data('rel'); //Trouver la pop-up correspondante
		var popWidth = $(this).data('width'); //Trouver la largeur

		//Faire apparaitre la pop-up et ajouter le bouton de fermeture
		$("#popup.popup_block").fadeIn().css({ 'width': popWidth}).prepend('<a href="#" class="close"><img src="img/delete.png" width="40px" class="btn_close" title="Close Window" alt="Close" /></a>');
		$("#popup.popup_block").css("display","block");
		//Récupération du margin
		var popMargTop = ($('#' + popID).height()) / 4;
		var popMargLeft = ($('#' + popID).width()) / 4;
		
		//Apply Margin to Popup
		$('#' + popID).css({ 
			'margin-top' : -popMargTop,
			'margin-left' : -popMargLeft
		}); 
		
		//Apparition du fond - .css({'filter' : 'alpha(opacity=80)'}) 
		$('body').append('<div id="fade"></div>');
		$('#fade').css({'filter' : 'alpha(opacity=80)'}).fadeIn();
		
		return false;
	});
	
	
	//Close Popups and Fade Layer
	$('body').on('click', 'a.close, #fade, #put', function() { //Au clic sur le body
		$('#fade , .popup_block').fadeOut(function() {
			$('#fade, a.close').remove();  
				
	}); //Disparition*
		return false;
	});
								});
							
				  });

$('#put').on('click', function() {  
	
	var prenom = $( "#Mod_prenom" ).val();
	var nom = $( "#Mod_nom" ).val();
    var civilite = $( "#Mod_civilite" ).val();
    var dateN = $( "#Mod_dateNaiss" ).val();
    var cp = $( "#Mod_cp" ).val();
    var mail = $( "#Mod_mail" ).val();
    var exclu = $("#Mod_checkExclu").is(':checked');
    console.log ("valeur de exclusion "+exclu);
    if (exclu == true) {
		exclu = "2200-01-01"
	}
	else exclu = null;
	var suivi = $("#Mod_checkMDE").is(':checked');
    var csp = parseInt($( "#Mod_csp").val());
    var formation = parseInt($( "#Mod_formation").val());
    var quartier = parseInt($( "#Mod_quartier").val());
    var visible = $("#Mod_visible").is(':checked');
    var id = parseInt($("#id_cache").attr('data-id'));
   
   
   
	var obj = {
			idUsagers : id,
			civiliteUsagers : civilite,
			cpUsagers : cp,
			dateNaissUsagers : dateN,
			exclusion : exclu,
			idCsp : csp,
			idNivFormation : formation,
			idQuartier : quartier,
			mailUsagers : mail,
			nomUsagers : nom,
			prenomUsagers : prenom,
			suiviMissionLocale : suivi,
			visibleUsagers : visible
	
			};
	console.log(obj);
	console.log(id);
	//console.log($('#id_cache').attr('data-id'));
console.log(JSON.stringify(obj));
		
	 $.ajax({
						
		 url : 'api/usagers/'+ $("#id_cache").attr("data-id"),
						//url : 'http://10.0.11.38:8080/TempoRest/api/usagers/'+ $("#id_cache").attr("data-id"),
						type : 'PUT',
						contentType : 'application/json; charset=UTF-8',
						crossDomain : true,
						//data : obj
						data : JSON.stringify(obj)
						
					}).success(function(data) {

						
							sessionStorage.setItem('idUsagers',id);
							location.reload();
								 }); 
						
						
					 }); 
});
 
