	/*----- CRUD des employés -----*/
$(document).ready(function(){
var v="";
var listeEmpl = [];
var listeSite = [];
var listeFonction = [];
var listeStructure = [];
$("#formulaire" ).hide();
$("#suppr").hide();
$('#creer').hide();

function refresh(){
if ('idEmploye' in sessionStorage){
	$.ajax({
//		url : 'http://10.0.10.138:8080/Cyberbase/api/employe',
		url : 'api/employe',
		type : 'GET',
		datatype : 'json',
		crossDomain : true 

	}).done(function(data) {
	for (ii=0; ii<data.length; ii++){
		  if (sessionStorage.getItem('idEmploye') == data[ii].idEmploye) {

			  //Remplissage des champs de consultation de la fiche Employe
					
			  $("#formulaire" ).show();
			  $( "#prenom" ).val( data[ii].prenomEmploye);
			  $( "#nom" ).val(data[ii].nomEmploye );
			  $( "#mdp" ).val( data[ii].motdepasse);
			  $( "#mail" ).val( data[ii].mailEmploye );
			  $( "#login" ).val( data[ii].loginEmploye );
			  
			  for (jj=0; jj <listeSite.length; jj++){ 
					if (listeSite[jj].label === data[ii].idSite){
						$( "#site" ).val( listeSite[jj].value);}
						console.log("boucle site du refresh");
				 };
			  for (kk=0; kk <listeFonction.length; kk++){ 
						if (listeFonction[kk].label === data[ii].idFonction){
							$( "#fonction" ).val( listeFonction[kk].value);}
					 };
			  for (aa=0; aa <listeStructure.length; aa++){ 
						if (listeStructure[aa].label === data[ii].idStructures){
							$( "#structure" ).val( listeStructure[aa].value)}
						console.log("boucle structure du refresh");
							;}
			  $('#suppr').show();
			  //Remplissage des champs de la modale de modification de la fiche usager
			  $("#id_cache").val(data[ii].idEmploye);
			  $( "#Mod_prenom" ).val( data[ii].prenomEmploye   );
			  $( "#Mod_nom" ).val(data[ii].nomEmploye );
			  $( "#Mod_mdp" ).val( data[ii].motdepasse );
			  $( "#Mod_mail" ).val( data[ii].mailEmploye );
			  $( "#Mod_login" ).val( data[ii].loginEmploye );

						  var sites ="";
						  
						  for (jj=0; jj <listeSite.length; jj++){ 

							  sites += '<option value="'
								  + listeSite[jj].label
								  + '"'
								  if (listeSite[jj].label === data[ii].idSite){
										sites += ' selected'  }
							  sites +='>'
								  + listeSite[jj].value
								  + '</option>'
										
							};
							$('#Mod_site').append( sites);

						  var fonctions="";
						  for (kk=0; kk <listeFonction.length; kk++){ 

							  fonctions += '<option value="'
								  + listeFonction[kk].label
								  + '"'
									if (listeFonction[kk].label === data[ii].idFonction){
										fonctions += ' selected' }
							  			fonctions +='>'
									  + listeFonction[kk].value
									  + '</option>'
								};
						  $('#Mod_fonction').append(fonctions);	
								
						  var structures ="";				 				 
						  for (aa=0; aa <listeStructure.length; aa++){ 
							  structures += '<option value="'
								  + listeStructure[aa].label
								  + '"'
								  if (listeStructure[aa].label === data[ii].idStructures){
									  structures += ' selected'}
							          structures +='>'
								  + listeStructure[aa].value
								  + '</option>'
									 };
						  $('#Mod_structure').append(structures);	
						  
						  $("#modifier").css("display","block");
						  var id = data[ii].idEmploye;
						  $("#id_cache").attr("data-id", id);
						  sessionStorage.clear()
		  		} 
			}
		})
	}
};
	
		
//récupération de la source JSON (fichier local ou distant)	
$.ajax({
//		url : 'http://10.0.10.138:8080/Cyberbase/api/site',
		url : 'api/site',
		type : 'GET',
		datatype : 'json',
		crossDomain : true 
		}).done(function (data) {
			for (ii=0; ii<data.length; ii++){
				listeSite.push({label:data[ii].idSite, value:data[ii].nomSite});
				}
			});
						
$.ajax({
//		url : 'http://10.0.10.138:8080/Cyberbase/api/fonction',
		url : 'api/fonction',
		type : 'GET',
		datatype : 'json',
		crossDomain : true 
		}).done(function (data) {
			for (ii=0; ii<data.length; ii++){
				listeFonction.push({label:data[ii].idFonction, value:data[ii].libelleFonction});
				}
			});

$.ajax({
//		url : 'http://10.0.10.138:8080/Cyberbase/api/structures',
		url : 'api/structures',
		type : 'GET',
		datatype : 'json',
		crossDomain : true 
		}).done(function (data) {
			for (ii=0; ii<data.length; ii++){
				listeStructure.push({label:data[ii].idStructures, value:data[ii].nomStructures});
				}
			});

$('#recherche').show(function() {
		$.ajax({

//				url : 'http://10.0.10.138:8080/Cyberbase/api/employe',
				url : 'api/employe',
				type : 'GET',
				datatype : 'json',
				crossDomain : true 
				}).done(function(data) {
				// Remplissage des listes déroulantes de la modale de création
						var sites ="";
						for (jj=0; jj <listeSite.length; jj++){ 
							sites += '<option value="'
							+ listeSite[jj].label
							+ '"'
							sites +='>'
							+ listeSite[jj].value
							+ '</option>'
							};
						$('#Crea_site').append(sites);

						var fonctions="";
						for (kk=0; kk <listeFonction.length; kk++){ 
							fonctions += '<option value="'
							+ listeFonction[kk].label
							+ '"'
							fonctions +='>'
							+ listeFonction[kk].value
							+ '</option>'
							};
						$('#Crea_fonction').append(fonctions);
												  
						var structures ="";				 				 
						for (aa=0; aa <listeStructure.length; aa++){ 
							structures += '<option value="'
							+ listeStructure[aa].label
							+ '"'
							structures +='>'
							+ listeStructure[aa].value
							+ '</option>'
							};
						$('#Crea_structure').append(structures);						

				//création de la source du champ d'autocomplétion (label et value)
				for (ii=0; ii<data.length; ii++){
					listeEmpl.push({label:data[ii].nomEmploye +' '+ data[ii].prenomEmploye, value:data[ii].idEmploye });
					}
				
				console.log("tableau chargé");
				$('#creer').show();
				refresh();
	//utilisation du plugin jquery d'autocomplétion 
    $("#recherche").autocomplete({source :listeEmpl, minLength : 2, select: function (event, ui) {
	//récupération des label/value
    var l = ui.item.label;
    var v= ui.item.value;

    // mise à jour de la valeur affichée
    $('#recherche').html(l);
    this.value = l; 
   console.log("valeur de l "+l);
   console.log("valeur de v "+v);
    

    //remplissage automatique des champs 
   for (ii=0; ii<data.length; ii++){
	   
		  if (v === data[ii].idEmploye) {

			  //Remplissage des champs de consultation de la fiche Employe
					
			  $("#formulaire" ).show();
			  $( "#prenom" ).val( data[ii].prenomEmploye);
			  $( "#nom" ).val(data[ii].nomEmploye );
			  $( "#mdp" ).val( data[ii].motdepasse);
			  $( "#mail" ).val( data[ii].mailEmploye );
			  $( "#login" ).val( data[ii].loginEmploye );
			  
			  for (jj=0; jj <listeSite.length; jj++){ 
				  if (listeSite[jj].label === data[ii].idSite){
					$( "#site" ).val( listeSite[jj].value);}
				 	};
				 	
			  for (kk=0; kk <listeFonction.length; kk++){ 
				  if (listeFonction[kk].label === data[ii].idFonction){
					$( "#fonction" ).val( listeFonction[kk].value);}
					 };
					 
			  for (aa=0; aa <listeStructure.length; aa++){ 
				  if (listeStructure[aa].label === data[ii].idStructures){
					$( "#structure" ).val( listeStructure[aa].value);}
					 };
			  $('#suppr').show();
			  
			  //Remplissage des champs de la modale de modification de la fiche employé
			  $("#id_cache").val(data[ii].idEmploye);
			  $( "#Mod_prenom" ).val( data[ii].prenomEmploye   );
			  $( "#Mod_nom" ).val(data[ii].nomEmploye );
			  $( "#Mod_mdp" ).val( data[ii].motdepasse );
			  $( "#Mod_mail" ).val( data[ii].mailEmploye );
			  $( "#Mod_login" ).val( data[ii].loginEmploye );

			  var sites ="";
			  for (jj=0; jj <listeSite.length; jj++){ 
				  sites += '<option value="'
				  + listeSite[jj].label
				  + '"'
				  if (listeSite[jj].label === data[ii].idSite){
					sites += ' selected'  }
				  sites +='>'
				  + listeSite[jj].value
				  + '</option>'
				  };
				$('#Mod_site').append(sites);

			  var fonctions="";
			  for (kk=0; kk <listeFonction.length; kk++){ 
				  fonctions += '<option value="'
				  + listeFonction[kk].label
				  + '"'
				  if (listeFonction[kk].label === data[ii].idFonction){
					fonctions += ' selected' }
				  fonctions +='>'
				  + listeFonction[kk].value
				  + '</option>'
				  };
				  $('#Mod_fonction').append(fonctions);
				  
			  var structures ="";				 				 
			  for (aa=0; aa <listeStructure.length; aa++){ 
				  structures += '<option value="'
				  + listeStructure[aa].label
				  + '"'
				  if (listeStructure[aa].label === data[ii].idStructures){
					structures += ' selected'}
				  structures +='>'
				  + listeStructure[aa].value
				  + '</option>'
						 };
				  $('#Mod_structure').append(structures);
			  
			  $("#modifier").css("display","block");
			  var id = data[ii].idEmploye;
			  $("#id_cache").attr("data-id", id);
				} 

				else{ $("#visible" ).css("display","block");}
				
						  }
 //désactivation de l'affichage de la value
   return false; 
	  }});



//création de la modale modification

	$('a.poplight').on('click', function() {
		var popID = $(this).data('rel'); //Trouver la pop-up correspondante
		var popWidth = $(this).data('width'); //Trouver la largeur
		
		//Faire apparaitre la pop-up et ajouter le bouton de fermeture
		$("#"+popID+".popup_block").fadeIn().css({ 'width': popWidth}).prepend('<a href="#" class="close"><img src="img/delete.png" width="40px" class="btn_close" title="Close Window" alt="Close" /></a>');
		$("#"+popID+".popup_block").css("display","block");
		
		//Récupération du margin
		var popMargTop = ($('#' + popID).height()) / 4;
		var popMargLeft = ($('#' + popID).width()) / 4;
		
		//Apply Margin to Popup
		$('#' + popID).css({ 
			'margin-top' : -popMargTop,
			'margin-left' : -popMargLeft
		}); 
		
		//Apparition du fond - .css({'filter' : 'alpha(opacity=80)'}) 
		$('body').append('<div id="fade"></div>');
		$('#fade').css({'filter' : 'alpha(opacity=80)'}).fadeIn();
		
		return false;
	});
	
	
	//Close Popups and Fade Layer
	$('body').on('click', 'a.close, #fade, #put', function() { //Au clic sur le body
		$('#fade , .popup_block').fadeOut(function() {
			$('#fade, a.close').remove();  
				
	}); //Disparition*
		return false;
	});
		});
							
	});

//---------------- SUPPRESSION -----------------------------
//Fenêtre de confirmation de la suppression d'un employé
$('#suppr').on('click',function(){
		prenom = $('#Mod_prenom').val();
		nom = $('#Mod_nom').val();
		$('#dialog-confirm').attr("title","Supprimer "+prenom+" "+nom);
	    $("#dialog-confirm" ).dialog({
	      resizable: false,
	      height:25,
	      width:400,
	      modal: true,
	      buttons: {
	        "OK": function() {
	          $.ajax({
//	 			  url : 'http://10.0.10.138:8080/Cyberbase/api/employe/'+ $("#id_cache").val(),
	        	  url : 'api/employe/'+ $("#id_cache").val(),
	        	  type : 'DELETE',
	        	  contentType : 'application/json; charset=UTF-8',
	        	  crossDomain : true,
	          })
	          $( this ).dialog( "close" );
	          location.reload();
	        },
	        Cancel: function() {
	          $( this ).dialog( "close" );
	        }
	      }
	    });
	console.log($('#id_cache').val());
});

//---------------- MODIFICATION -----------------------------
						
$('#put').on('click', function() {  
	
	var idE = $("#id_cache").val();
	var prenom = $( "#Mod_prenom" ).val();
	var nom = $( "#Mod_nom" ).val();
    var mdp = $( "#Mod_mdp" ).val();
    var mail = $( "#Mod_mail" ).val();
    var login = $( "#Mod_login" ).val();
    var fonction = parseInt($( "#Mod_fonction").val());
    var structure = parseInt($( "#Mod_structure").val());
    var site = parseInt($( "#Mod_site").val());
    
	console.log(idE);
	console.log("--- "+structure+" --- ");
   
	var obj = {
			idEmploye : idE,
			nomEmploye : nom,
			prenomEmploye : prenom,
			motdepasse : mdp,
			mailEmploye : mail,
			loginEmploye : login,
			idFonction : fonction,
			idStructures : structure,
			idSite : site,
			};
	
	 $.ajax({
//		 	url : 'http://10.0.10.138:8080/Cyberbase/api/employe/'+ $("#id_cache").attr("data-id"),
			url : 'api/employe/'+ $("#id_cache").attr("data-id"),
			type : 'PUT',
			contentType : 'application/json; charset=UTF-8',
			crossDomain : true,
			data : JSON.stringify(obj)
			}).success(function(data) {
				sessionStorage.setItem('idEmploye',idE);
				location.reload();
					 }); 
			});

//---------------- CREATION -----------------------------
$('#post').on('click',function(){
	
//	Affectation des valeurs saisies dans des variables à envoyer pour la création
	var prenom = $( "#Crea_prenom" ).val();
	var nom = $( "#Crea_nom" ).val();
    var mdp = $( "#Crea_mdp" ).val();
    var mail = $( "#Crea_mail" ).val();
    var login = $( "#Crea_login" ).val();
    var fonction = parseInt($( "#Crea_fonction").val());
    var structure = parseInt($( "#Crea_structure").val());
    var site = parseInt($( "#Crea_site").val());
    if (mail === "") {
		mail = null;
	};
	if (login === "") {
		login = null;
	};
    var objCrea = {
			nomEmploye : nom,
			prenomEmploye : prenom,
			motdepasse : mdp,
			mailEmploye : mail,
			loginEmploye : login,
			idFonction : fonction,
			idStructures : structure,
			idSite : site,
			};
    
	 $.ajax({
//			url : 'http://10.0.10.138:8080/Cyberbase/api/employe',
			url : 'api/employe',
			type : 'POST',
			contentType : 'application/json; charset=UTF-8',
			crossDomain : true,
			data : JSON.stringify(objCrea)
			}).done (function() {
				location.reload();
			})
		})
 }); 