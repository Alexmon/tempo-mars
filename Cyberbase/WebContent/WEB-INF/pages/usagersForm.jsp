	
	<div id="main">
 <script type="text/javascript" src="js/autoUsagers.js"></script> 
		<div id="titrePage">
		<h2>Usagers de la Maison de l'emploi </h2>
		</div>
			<div id="usagersDteExpli">
				<p class="titrePara">::: Memo :::</p>
					<p>Sur cette page, vous avez la possibilit� de voir les informations relatives a
					 un usagers des Maison de l'emploi, et si besoin est, de modifier celles-ci.</p>
					<p>Pour trouver l'usagers recherch�, entrez les deux premi�res lettres de son nom ou de son pr�nom dans le champ pr�vu a cet effet.</p>
					<p>Pour le modifier, veuillez cliquer sur le bouton ��Modifier��. La case ��Suivi Maison de l'Emploi�� indique si la personne fait l'objet d'un suivi par les services de la Maison de l'Emploi. La case ��exclusion�� indique si l'usager est exclu des services, enfin la case ��visible�� permet de ne plus voir l'usager dans l'application (ses informations sont n�anmoins conserv�es dans la base de donn�es).</p>
			</div>
			
			
			<div id="formulaireUsagers">
			<input class="ui-autocomplete-input" id="recherche"
				placeholder="Rechercher un nom">
			<br>
						<div id="formulaire">
						<form>
					<label class="civilite">Civilit� : </label> <input class="civilite"
						id="civilite" disabled="true"> <br> <label
						class="prenom">Pr�nom : </label><input class="prenom" id="prenom"
						disabled="true"> <br> <label class="nom">Nom : </label><input
						class="nom" id="nom" disabled="true"> <br> <label
						class="dateNaiss">Date de naissance : </label><input
						class="dateNaiss" id="dateNaiss" disabled="true"> <br> <label
						class="cp">Code postal : </label><input class="cp" id="cp"
						disabled="true"> <br> <label class="mail">Courriel
						: </label><input class="mail" id="mail" disabled="true"> <br> 
						<label>CSP : </label><input
							id="csp" disabled="true"> <br> <label>Formation : </label><input
							id="formation" disabled="true"> <br> <label>Quartier : </label><input
							id="quartier" disabled="true"> <br>
						
						<label
						class="checkMDE">Suivi Maison de l'emploi : <input
						type="checkbox" class="checkMDE" id="checkMDE" disabled="true"></label>
					<label class="checkExclu">Exclusion : <input type="checkbox"
						class="checkExclu" id="checkExclu" disabled="true"></label>
				</form>
</div> 
	<div id="visible">Cet usager a �t� supprim�. Veuillez contacter
		votre administrateur.</div>

	<a href="#" id="modifier" data-width="500" data-rel="popup1"
		class="poplight">Modifier la fiche usager</a>

	<div id="popup" class="popup_block">
		<div id="titrePop">Modification de l'usager</div>
		<form id="Mod_formulaire">
			<input id="id_cache"> <label>Civilit� : </label> <input
				id="Mod_civilite"> <br> <label>Pr�nom : </label><input
				id="Mod_prenom"> <br> <label>Nom : </label><input
				id="Mod_nom"> <br> <label>Date de naissance : </label><input
				id="Mod_dateNaiss"> <br> <label>Code postal : </label><input
				id="Mod_cp"> <br> <label>Courriel : </label><input
				id="Mod_mail"> <br> 
				<label>CSP : </label><select name="Mod_csp" id="Mod_csp"> </select>
				<label>Formation : </label><select name="Mod_formation" id="Mod_formation"></select> 
				<label>Quartier : </label><select name="Mod_quartier" id="Mod_quartier"></select><br> 
				
				<div id="basPop">
					<label>Suivi Maison de l'emploi : <input type="checkbox" id="Mod_checkMDE">
					</label> <label>Exclusion : <input type="checkbox"
					id="Mod_checkExclu"></label> <label>Visible : <input
					type="checkbox" id="Mod_visible"></label>
				</div>
			<button type="submit" id="put">Enregistrer les modifications</button>
			<input type="reset" value="Remise � z�ro">
		</form>
	</div>
</div>

	<article id="secRightpages">
            <a href="sites.jsp"><div class="boutonRight" id="sitesRight">Sites</div></a>
            <a href="usagers.jsp"><div class="boutonRight" id="usagersRight">Usagers</div></a>
            <a href="statistiques.jsp"><div class="boutonRight" id="statistiquesRight">Statistiques</div></a>
		</article>
</div>