 <%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>

<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!doctype html>

<html lang="fr">
	<head>
	    <title>:::CyberBase - Tempo:::</title>
	    <meta http-equiv="content-type" content="text/html; charset=utf-8" />
	    <meta name="author" content="Tempo_v1">
	    <link rel="stylesheet" href="css/styles.css">
	    <link rel="stylesheet" href="css/Tempo_css.css">
	    <link rel="stylesheet" type="text/css" href="css/jquery-ui-1.10.4.css">
	    <link rel="stylesheet" type="text/css" href="css/jquery-ui-1.10.4.min.css">
	    <link rel="icon" href="img/favicon.ico" type="image/x-icon" />
	    <link rel="shortcut icon" href="img/favicon.ico" type="image/x-icon" />
	   	<script src="js/jquery.js"></script> 
	   	<script src="js/jquery.min.js"></script> 
	   	<script src="js/jquery-ui-1.10.4.min.js"></script> 
	   	<script src="js/jquery-ui-1.10.4.custom.min.js"></script> 
	   	</head>

	<body>
 
 
  <header id="header">
        <span><a href="index.jsp"><img id="logo" src="img/logo-cyberbase-blc.png"/></a></span>
        <span class="blcRight">numero support : 08 08 08 08 08</span>
        <span class="userConnected" id="headerUser"></span>
  </header>