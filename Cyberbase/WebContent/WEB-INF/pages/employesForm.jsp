<div id="main">
<script type="text/javascript" src="js/employe.js"></script>
<div id="titrePage">
		<h2>Employ�s de la Maison de l'emploi </h2>
		</div>
			<div id="usagersDteExpli">
				<p class="titrePara">::: Memo :::</p>
					<p>Sur cette page, vous avez la possibilit� de voir les informations relatives �
					 un employ�, et si besoin est, de modifier celles-ci.</p>
					<p>Pour trouver l'employ� recherch�, entrez les deux premi�res lettres de son nom ou de son pr�nom dans le champ pr�vu a cet effet.</p>
					<p>Pour le modifier, veuillez cliquer sur le bouton ��Modifier��.</p>
			</div>
<div id="formulaireUsagers">
	<input class="ui-autocomplete-input" id="recherche"
		placeholder="Rechercher un nom">
	<br>
	<div id="formulaire">
	<form>
<!-- 		<label class="civilite">Civilit� : </label> <input class="civilite"id="civilite" disabled="true"> <br>  -->
		<label class="prenom">Pr�nom : </label><input class="prenom" id="prenom" disabled="true"> <br> 
		<label class="nom">Nom : </label><input class="nom" id="nom" disabled="true"> <br> 
		<label class="mdp">Mot de Passe : </label> <input class="mdp" id="mdp" disabled="true"> <br> 
		<label class="mail">Courriel : </label><input class="mail" id="mail" disabled="true"> <br> 
		<label class="login">Login : </label><input class="login" id="login" disabled="true"> <br> 
		<label>Fonction : </label><input id="fonction" disabled="true"> <br> 
		<label>Site : </label><input id="site" disabled="true"> <br> 
		<label>Structure de rattachement : </label><input id="structure" disabled="true"> <br>
	</form>
</div> 

	<a href="#" id="modifier" data-width="500" data-rel="popupModif"
		class="poplight">Modifier la fiche employ�</a>
	<a href="#"id="suppr">Supprimer l'employ�</a>
		<br>
	<a href="#" id="creer" data-width="500" data-rel="popupCrea"
		class="poplight">Cr�ation d'un employ�</a>
		
	<!-- 	div de la modale de cr�ation -->	
	<div id="popupModif" class="popup_block">
		<h2>Modification de l'employ�</h2>
		<form id="Mod_formulaire">
			<input id="id_cache"> 
			<label>Pr�nom : </label> <input id="Mod_prenom"> <br> 
			<label>Nom : </label> <input id="Mod_nom"> <br> 
			<label>Mot de Passe : </label> <input id="Mod_mdp"> <br> 
			<label>Mail : </label> <input id="Mod_mail"> <br> 
			<label>Login : </label> <input id="Mod_login"> <br> 
			<label>Fonction : </label><select name="Mod_fonction" id="Mod_fonction"> </select><br> 
			<label>Site : </label><select name="Mod_site" id="Mod_site"></select><br> 
			<label>Structure : </label> <select name="Mod_structure" id="Mod_structure"></select><br> 
			<button type="submit" id="put">Enregistrer les modifications</button>
			<input type="reset" value="Remise � z�ro">
		</form>
	</div>
	
<!-- 	div de la modale de cr�ation -->
	<div id="popupCrea" class="popup_block">
		<h2>Cr�ation d'un employ�</h2>
		<form id="Crea_formulaire">
			<label>Pr�nom : </label> <input id="Crea_prenom"> <br> 
			<label>Nom : </label> <input id="Crea_nom"> <br> 
			<label>Mot de Passe : </label> <input id="Crea_mdp"> <br> 
			<label>Mail : </label> <input id="Crea_mail"> <br> 
			<label>Login : </label> <input id="Crea_login"> <br> 
			<label>Fonction : </label><select name="Crea_fonction" id="Crea_fonction"> </select><br> 
			<label>Site : </label><select name="Crea_site" id="Crea_site"></select><br> 
			<label>Structure : </label> <select name="Crea_structure" id="Crea_structure"></select><br> 
			<button type="submit" id="post">Cr�ation de l'employ�</button>
			<input type="reset" value="Remise � z�ro">
		</form>
	</div>
	
<!-- 	div affichant la boite de dialogue de suppression -->
	<div id="dialog-confirm">
 	</div>
 </div>	
 	<article id="secRightpages">
            <a href="sites.jsp"><div class="boutonRight" id="sitesRight">Sites</div></a>
            <a href="usagers.jsp"><div class="boutonRight" id="usagersRight">Usagers</div></a>
            <a href="statistiques.jsp"><div class="boutonRight" id="statistiquesRight">Statistiques</div></a>
		</article>
 </div>	