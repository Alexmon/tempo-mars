<div id="main">
		<div id="titrePage">
		<h2>Statistiques Cyberbase</h2>
		
		<div id="BtnTelechar">Statistiques compl�tes au format tableur <br>
		<button id="boutonconnexion" class="btnTele">T�l�charger</button></div>
		</div>
		
		

		<div  id="graph1">
		<h3>Nombre d'usagers par quartier</h3>
		<canvas id="provenance" style=" height:250px; width:250px" ></canvas>
		 </div> 
		

	<div id="graph2" >
		<h3>Visites par mois et ann�es pour l'ensemble des sites</h3>
		 <canvas id="annees" style=" height:210px; width:250px"></canvas> 
		 <div id="Legendaire" style=" margin-left:10%; margin-right:10px;"></div>
		</div> 

	<div id="graph3">
		<h3>Motifs des connexions</h3>
		 <canvas id="motif" style=" height:150px; width:150px"></canvas> 
		</div> 
	
		<article id="secRightpages">
            <a href="sites.jsp"><div class="boutonRight" id="sitesRight">Sites</div></a>
            <a href="usagers.jsp"><div class="boutonRight" id="usagersRight">Usagers</div></a>
            <a href="statistiques.jsp"><div class="boutonRight" id="statistiquesRight">Statistiques</div></a>
		</article>
</div>

<script type="text/javascript" src="js/jquery.min.js"></script>
<script type="text/javascript" src="js/legend.js"></script>
<script type="text/javascript" src="js/excel.js"></script>
<script type="text/javascript" src="js/Chart.js"></script>
<script type="text/javascript" src="js/canvas.js"></script>