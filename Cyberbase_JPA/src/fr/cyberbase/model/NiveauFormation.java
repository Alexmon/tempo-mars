package fr.cyberbase.model;

import java.io.Serializable;

import javax.persistence.*;


/**
 * The persistent class for the "csp" database table.
 * 
 */
@Entity
@Table(name="niveau_formation")
@NamedQuery(name="NiveauFormation.findAll", query="SELECT l FROM NiveauFormation l")
public class NiveauFormation implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="id_niv_formation")
	private Integer idNivFormation;

	@Column(name="lib_niv_formation")
	private String libNivFormation;

	public NiveauFormation() {
	}

	public Integer getidNivFormation() {
		return this.idNivFormation;
	}

	public void setidNivFormation(Integer idNivFormation) {
		this.idNivFormation = idNivFormation;
	}

	public String getLibNivFormation() {
		return this.libNivFormation;
	}

	public void setLibNivFormation(String libNivFormation) {
		this.libNivFormation = libNivFormation;
	}

}