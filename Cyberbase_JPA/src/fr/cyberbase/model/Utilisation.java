package fr.cyberbase.model;

import java.io.Serializable;
import javax.persistence.*;
import java.sql.Time;
import java.util.Date;


/**
 * The persistent class for the "utilisation" database table.
 * 
 */
@Entity
@Table(name="utilisation")
@NamedQuery(name="Utilisation.findAll", query="SELECT u FROM Utilisation u")
public class Utilisation implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="id_utilisation")
	private int idUtilisation;

	@Temporal(TemporalType.DATE)
	@Column(name="date_utilisation")
	private Date dateUtilisation;

	@Column(name="heure_deb_utilisation")
	private Time heureDebUtilisation;

	@Column(name="heure_fin_utilisation")
	private Time heureFinUtilisation;

	@Column(name="id_motif")
	private int idMotif;

	@Column(name="id_poste")
	private int idPoste;

	@Column(name="id_usagers")
	private int idUsagers;

	public Utilisation() {
	}

	public int getIdUtilisation() {
		return this.idUtilisation;
	}

	public void setIdUtilisation(int idUtilisation) {
		this.idUtilisation = idUtilisation;
	}

	public Date getDateUtilisation() {
		return this.dateUtilisation;
	}

	public void setDateUtilisation(Date dateUtilisation) {
		this.dateUtilisation = dateUtilisation;
	}

	public Time getHeureDebUtilisation() {
		return this.heureDebUtilisation;
	}

	public void setHeureDebUtilisation(Time heureDebUtilisation) {
		this.heureDebUtilisation = heureDebUtilisation;
	}

	public Time getHeureFinUtilisation() {
		return this.heureFinUtilisation;
	}

	public void setHeureFinUtilisation(Time heureFinUtilisation) {
		this.heureFinUtilisation = heureFinUtilisation;
	}

	public int getIdMotif() {
		return this.idMotif;
	}

	public void setIdMotif(int idMotif) {
		this.idMotif = idMotif;
	}

	public int getIdPoste() {
		return this.idPoste;
	}

	public void setIdPoste(int idPoste) {
		this.idPoste = idPoste;
	}

	public int getIdUsagers() {
		return this.idUsagers;
	}

	public void setIdUsagers(int idUsagers) {
		this.idUsagers = idUsagers;
	}

	

}