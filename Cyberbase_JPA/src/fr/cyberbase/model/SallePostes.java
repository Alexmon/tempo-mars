package fr.cyberbase.model;

import java.io.Serializable;
import java.util.List;

import javax.persistence.*;


/**
 * The persistent class for the "salle" database table.
 * 
 */
@Entity
@Table(name="salle")
@NamedQuery(name="SallePostes.findAll", query="SELECT s FROM SallePostes s")
public class SallePostes implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="id_salle")
	private int idSalle;

	@OneToMany(mappedBy="salle", fetch=FetchType.EAGER)
	private List<Poste> poste;
		
	public SallePostes() {
	}

	public int getIdSalle() {
		return this.idSalle;
	}

	public void setIdSalle(int idSalle) {
		this.idSalle = idSalle;
	}
	
	public List<Poste> getPoste() {
		return this.poste;
	}
}