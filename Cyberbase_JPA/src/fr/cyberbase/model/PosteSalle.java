package fr.cyberbase.model;

import java.io.Serializable;

import javax.persistence.*;


/**
 * The persistent class for the "poste" database table.
 * 
 */
@Entity
@Table(name="poste")
@NamedQuery(name="PosteSalle.findAll", query="SELECT p FROM PosteSalle p")
public class PosteSalle implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="id_poste")
	private int idPoste;

	@Column(name="nom_poste")
	private String nomPoste;
	
	@Column(name="visible_poste")
	private Boolean visiblePoste;
	
	//bi-directional many-to-one association to Salle
	@ManyToOne
	@JoinColumn(name = "id_salle")
	private Salle salle;

	public PosteSalle() {
	}

	public int getIdPoste() {
		return this.idPoste;
	}

	public void setIdPoste(int idPoste) {
		this.idPoste = idPoste;
	}

	public String getNomPoste() {
		return this.nomPoste;
	}

	public void setNomPoste(String nomPoste) {
		this.nomPoste = nomPoste;
	}

	public Boolean getVisiblePoste() {
		return visiblePoste;
	}

	public void setVisiblePoste(Boolean visiblePoste) {
		this.visiblePoste = visiblePoste;
	}

	public Salle getSalle() {
		return this.salle;
	}

	public void setSalle(Salle salle) {
		this.salle = salle;
	}

}