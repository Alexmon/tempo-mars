package fr.cyberbase.model;

import java.io.Serializable;
import javax.persistence.*;


/**
 * The persistent class for the "fonction" database table.
 * 
 */
@Entity
@Table(name="fonction")
@NamedQuery(name="Fonction.findAll", query="SELECT f FROM Fonction f")
public class Fonction implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="id_fonction")
	private int idFonction;

	@Column(name="libelle_fonction")
	private String libelleFonction;

	public Fonction() {
	}

	public int getIdFonction() {
		return this.idFonction;
	}

	public void setIdFonction(int idFonction) {
		this.idFonction = idFonction;
	}

	public String getLibelleFonction() {
		return this.libelleFonction;
	}

	public void setLibelleFonction(String libelleFonction) {
		this.libelleFonction = libelleFonction;
	}

}