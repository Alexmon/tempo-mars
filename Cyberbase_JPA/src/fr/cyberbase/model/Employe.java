package fr.cyberbase.model;

import java.io.Serializable;

import javax.persistence.*;


/**
 * The persistent class for the "employe" database table.
 * 
 */
@Entity
@Table(name="employe")
@NamedQuery(name="Employe.findAll", query="SELECT e FROM Employe e")
public class Employe implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="id_employe")
	private int idEmploye;

	@Column(name="nom_employe")
	private String nomEmploye;

	@Column(name="prenom_employe")
	private String prenomEmploye;
	
	@Column(name="motdepasse")
	private String motdepasse;
	
	@Column(name="mail_employe")
	private String mailEmploye;

	@Column(name="login_employe")
	private String loginEmploye;
	
	@Column(name="id_fonction")
	private int idFonction;

	@Column(name="id_site")
	private int idSite;

	@Column(name="id_structures")
	private int idStructures;

	

	//uni-directional many-to-one association to Fonction
	
//	@ManyToOne
//	@JoinColumn (name = "id_fonction")
//	private Fonction fonction;

	public Employe() {
	}

	public int getIdEmploye() {
		return this.idEmploye;
	}

	public void setIdEmploye(int idEmploye) {
		this.idEmploye = idEmploye;
	}

	public int getIdFonction() {
		return this.idFonction;
	}

	public void setIdFonction(int idFonction) {
		this.idFonction = idFonction;
	}

	public int getIdSite() {
		return this.idSite;
	}

	public void setIdSite(int idSite) {
		this.idSite = idSite;
	}

	public int getIdStructures() {
		return this.idStructures;
	}

	public void setIdStructures(int idStructures) {
		this.idStructures = idStructures;
	}

	public String getMotdepasse() {
		return this.motdepasse;
	}

	public void setMotdepasse(String motdepasse) {
		this.motdepasse = motdepasse;
	}

	public String getNomEmploye() {
		return this.nomEmploye;
	}

	public void setNomEmploye(String nomEmploye) {
		this.nomEmploye = nomEmploye;
	}

	public String getPrenomEmploye() {
		return this.prenomEmploye;
	}

	public void setPrenomEmploye(String prenomEmploye) {
		this.prenomEmploye = prenomEmploye;
	}
	
	public String getMailEmploye() {
		return mailEmploye;
	}

	public void setMailEmploye(String mailEmploye) {
		this.mailEmploye = mailEmploye;
	}

	public String getLoginEmploye() {
		return loginEmploye;
	}

	public void setLoginEmploye(String loginEmploye) {
		this.loginEmploye = loginEmploye;
	}

//	public Fonction getFonction() {
//		return this.fonction;
//	}

//	public void setFonction(Fonction fonction) {
//		this.fonction = fonction;
//	}

}