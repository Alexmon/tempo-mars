package fr.cyberbase.model;

import java.io.Serializable;

import javax.persistence.*;

/**
 * The persistent class for the "structures" database table.
 * 
 */
@Entity
@Table(name = "structures")
@NamedQuery(name = "Structures.findAll", query = "SELECT l FROM Structures l")
public class Structures implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id_structures")
	private Integer idStructures;

	@Column(name = "nom_structures")
	private String nomStructures;

	@Column(name = "adresse_structures")
	private String adresseStructures;

	@Column(name = "cp_structures")
	private String cpStructures;

	@Column(name = "ville_structures")
	private String villeStructures;

	public String getAdresseStructures() {
		return adresseStructures;
	}
	
	public Structures() {
	}


	public void setAdresseStructures(String adresseStructures) {
		this.adresseStructures = adresseStructures;
	}

	public String getCpStructures() {
		return cpStructures;
	}

	public void setCpStructures(String cpStructures) {
		this.cpStructures = cpStructures;
	}

	public String getVilleStructures() {
		return villeStructures;
	}

	public void setVilleStructures(String villeStructures) {
		this.villeStructures = villeStructures;
	}

	public Integer getIdStructures() {
		return this.idStructures;
	}

	public void setIdStructures(Integer idStructures) {
		this.idStructures = idStructures;
	}

	public String getNomStructures() {
		return this.nomStructures;
	}

	public void setNomStructures(String nomStructures) {
		this.nomStructures = nomStructures;
	}

}