package fr.cyberbase.model;

import java.io.Serializable;
import javax.persistence.*;
import java.util.List;


/**
 * The persistent class for the "motif" database table.
 * 
 */
@Entity
@Table(name="motif")
@NamedQuery(name="Motif.findAll", query="SELECT m FROM Motif m")
public class Motif implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="id_motif")
	private int idMotif;

	@Column(name="motif")
	private String motif;

	//bi-directional many-to-one association to Utilisation


	public Motif() {
	}

	public int getIdMotif() {
		return this.idMotif;
	}

	public void setIdMotif(int idMotif) {
		this.idMotif = idMotif;
	}

	public String getMotif() {
		return this.motif;
	}

	public void setMotif(String motif) {
		this.motif = motif;
	}

	
}