package fr.cyberbase.model;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;
import java.util.List;


/**
 * The persistent class for the "usagers" database table.
 * 
 */
@Entity
@Table(name="usagers")
@NamedQuery(name="Usager.findAll", query="SELECT u FROM Usager u")
public class Usager implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="id_usagers")
	private int idUsagers;

	@Column(name="civilite_usagers")
	private String civiliteUsagers;

	@Column(name="cp_usagers")
	private String cpUsagers;

	@Temporal(TemporalType.DATE)
	@Column(name="date_naiss_usagers")
	private Date dateNaissUsagers;

	@Temporal(TemporalType.DATE)
	@Column(name="exclusion")
	private Date exclusion;

	@Column(name="id_csp")
	private int idCsp;

	@Column(name="id_niv_formation")
	private int idNivFormation;

	@Column(name="id_quartier")
	private int idQuartier;

	@Column(name="mail_usagers")
	private String mailUsagers;

	@Column(name="nom_usagers")
	private String nomUsagers;

	@Column(name="prenom_usagers")
	private String prenomUsagers;

	@Column(name="suivi_mission_locale")
	private boolean suiviMissionLocale;
	
	@Column(name="visible_usagers")
	private boolean visibleUsagers;


	public Usager() {
	}

	public int getIdUsagers() {
		return this.idUsagers;
	}

	public void setIdUsagers(int idUsagers) {
		this.idUsagers = idUsagers;
	}

	public String getCiviliteUsagers() {
		return this.civiliteUsagers;
	}

	public void setCiviliteUsagers(String civiliteUsagers) {
		this.civiliteUsagers = civiliteUsagers;
	}

	public String getCpUsagers() {
		return this.cpUsagers;
	}

	public void setCpUsagers(String cpUsagers) {
		this.cpUsagers = cpUsagers;
	}

	public Date getDateNaissUsagers() {
		return this.dateNaissUsagers;
	}

	public void setDateNaissUsagers(Date dateNaissUsagers) {
		this.dateNaissUsagers = dateNaissUsagers;
	}

	public Date getExclusion() {
		return this.exclusion;
	}

	public void setExclusion(Date exclusion) {
		this.exclusion = exclusion;
	}

	public int getIdCsp() {
		return this.idCsp;
	}

	public void setIdCsp(int idCsp) {
		this.idCsp = idCsp;
	}

	public int getIdNivFormation() {
		return this.idNivFormation;
	}

	public void setIdNivFormation(int idNivFormation) {
		this.idNivFormation = idNivFormation;
	}

	public int getIdQuartier() {
		return this.idQuartier;
	}

	public void setIdQuartier(int idQuartier) {
		this.idQuartier = idQuartier;
	}

	public String getMailUsagers() {
		return this.mailUsagers;
	}

	public void setMailUsagers(String mailUsagers) {
		this.mailUsagers = mailUsagers;
	}

	public String getNomUsagers() {
		return this.nomUsagers;
	}

	public void setNomUsagers(String nomUsagers) {
		this.nomUsagers = nomUsagers;
	}

	public String getPrenomUsagers() {
		return this.prenomUsagers;
	}

	public void setPrenomUsagers(String prenomUsagers) {
		this.prenomUsagers = prenomUsagers;
	}

	public boolean getSuiviMissionLocale() {
		return this.suiviMissionLocale;
	}

	public void setSuiviMissionLocale(boolean suiviMissionLocale) {
		this.suiviMissionLocale = suiviMissionLocale;
	}

	public boolean isVisibleUsagers() {
		return visibleUsagers;
	}

	public void setVisibleUsagers(boolean visibleUsagers) {
		this.visibleUsagers = visibleUsagers;
	}

	
}