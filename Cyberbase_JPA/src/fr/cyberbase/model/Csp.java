package fr.cyberbase.model;

import java.io.Serializable;

import javax.persistence.*;


/**
 * The persistent class for the "csp" database table.
 * 
 */
@Entity
@Table(name="csp")
@NamedQuery(name="Csp.findAll", query="SELECT c FROM Csp c")
public class Csp implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="id_csp")
	private Integer idCsp;

	@Column(name="csp")
	private String csp;

	public Csp() {
	}

	public Integer getIdCsp() {
		return this.idCsp;
	}

	public void setIdCsp(Integer idCsp) {
		this.idCsp = idCsp;
	}

	public String getCsp() {
		return this.csp;
	}

	public void setCsp(String csp) {
		this.csp = csp;
	}

}