package fr.cyberbase.model;

import java.io.Serializable;
import java.util.List;

import javax.persistence.*;


/**
 * The persistent class for the "site" database table.
 * 
 */
@Entity
@Table(name="site")
@NamedQuery(name="SiteSalles.findAll", query="SELECT s FROM SiteSalles s")
public class SiteSalles implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="id_site")
	private int idSite;
	
	@OneToMany(mappedBy="site", fetch=FetchType.EAGER)
	private List<Salle> salle;
		
	public SiteSalles() {
	}

	public int getIdSite() {
		return this.idSite;
	}

	public void setIdSite(int idSite) {
		this.idSite = idSite;
	}

	public List<Salle> getSalle() {
		return this.salle;
	}
}