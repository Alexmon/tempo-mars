package fr.cyberbase.model;

import java.io.Serializable;

import javax.persistence.*;

/**
 * The persistent class for the "localisation" database table.
 * 
 */
@Entity
@Table(name = "localisation")
@NamedQuery(name = "Localisation.findAll", query = "SELECT l FROM Localisation l")
public class Localisation implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id_quartier")
	private Integer idQuartier;

	@Column(name = "nom_quartier")
	private String nomQuartier;

	public Localisation() {
	}

	public Integer getIdQuartier() {
		return idQuartier;
	}

	public void setIdQuartier(Integer idQuartier) {
		this.idQuartier = idQuartier;
	}

	public String getNomQuartier() {
		return this.nomQuartier;
	}

	public void setNomQuartier(String nomQuartier) {
		this.nomQuartier = nomQuartier;
	}

}