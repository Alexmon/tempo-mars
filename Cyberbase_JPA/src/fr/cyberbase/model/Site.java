package fr.cyberbase.model;

import java.io.Serializable;
import javax.persistence.*;


/**
 * The persistent class for the "site" database table.
 * 
 */
@Entity
@Table(name="site")
@NamedQuery(name="Site.findAll", query="SELECT s FROM Site s")
public class Site implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="id_site")
	private int idSite;

	@Column(name="adresse_site")
	private String adresseSite;

	@Column(name="cp_site")
	private String cpSite;

	@Column(name="nom_site")
	private String nomSite;

	@Column(name="telephone_site")
	private String telephoneSite;

	@Column(name="ville_site")
	private String villeSite;

	public Site() {
	}

	public int getIdSite() {
		return this.idSite;
	}

	public void setIdSite(int idSite) {
		this.idSite = idSite;
	}

	public String getAdresseSite() {
		return this.adresseSite;
	}

	public void setAdresseSite(String adresseSite) {
		this.adresseSite = adresseSite;
	}

	public String getCpSite() {
		return this.cpSite;
	}

	public void setCpSite(String cpSite) {
		this.cpSite = cpSite;
	}

	public String getNomSite() {
		return this.nomSite;
	}

	public void setNomSite(String nomSite) {
		this.nomSite = nomSite;
	}

	public String getTelephoneSite() {
		return this.telephoneSite;
	}

	public void setTelephoneSite(String telephoneSite) {
		this.telephoneSite = telephoneSite;
	}

	public String getVilleSite() {
		return this.villeSite;
	}

	public void setVilleSite(String villeSite) {
		this.villeSite = villeSite;
	}

}