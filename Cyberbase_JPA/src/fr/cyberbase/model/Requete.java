package fr.cyberbase.model;

import java.io.Serializable;

import javax.persistence.*;


/**
 * The persistent class for the "requete" database table.
 * 
 */
@Entity
@Table(name="requete")
@NamedQuery(name="Requete.findAll", query="SELECT r FROM Requete r")
public class Requete implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="id_requete")
	private Integer idRequete;

	@Column(name="nom_requete")
	private String nomRequete;
	
	@Column(name="requete")
	private String requete;

	public Requete() {
	}
	
	public String getNomRequete() {
		return nomRequete;
	}

	public void setNomRequete(String nomRequete) {
		this.nomRequete = nomRequete;
	}

	public String getRequete() {
		return requete;
	}

	public void setRequete(String requete) {
		this.requete = requete;
	}
	
	public Integer getIdRequete() {
		return this.idRequete;
	}

	public void setIdRequete(Integer idRequete) {
		this.idRequete = idRequete;
	}



}