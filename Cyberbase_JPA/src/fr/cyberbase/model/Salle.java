package fr.cyberbase.model;

import java.io.Serializable;

import javax.persistence.*;


/**
 * The persistent class for the "salle" database table.
 * 
 */
@Entity
@Table(name="salle")
@NamedQuery(name="Salle.findAll", query="SELECT s FROM Salle s")
public class Salle implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="id_salle")
	private int idSalle;

	@Column(name="nom_salle")
	private String nomSalle;
	
	//bi-directional many-to-one association to Site
	@ManyToOne
	@JoinColumn(name = "id_site")
	private Site site;

	public Salle() {
	}

	public int getIdSalle() {
		return this.idSalle;
	}

	public void setIdSalle(int idSalle) {
		this.idSalle = idSalle;
	}

	
	public Site getSite() {
		return this.site;
	}

	public void setSite(Site site) {
		this.site = site;
	}

	public String getNomSalle() {
		return nomSalle;
	}

	public void setNomSalle(String nomSalle) {
		this.nomSalle = nomSalle;
	}

}