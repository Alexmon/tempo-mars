package fr.imie.service;

import java.util.List;

import javax.ejb.Local;

import fr.cyberbase.model.Poste;

@Local
public interface PosteServiceLocal {

	List<Poste> findAll();

	void deletePoste(Poste poste);

	Poste updatePoste(Poste poste);

	Poste createPoste(Poste poste);

	Poste findPosteById(Poste poste);

}
