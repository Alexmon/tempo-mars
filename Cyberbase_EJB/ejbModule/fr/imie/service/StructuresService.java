package fr.imie.service;

import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import fr.cyberbase.model.Structures;

/**
 * Session Bean implementation class StructuresService
 */
@Stateless
public class StructuresService implements StructuresServiceLocal {

	@PersistenceContext
	EntityManager entityManager;

	/**
	 * Default constructor.
	 */
	public StructuresService() {
		// TODO Auto-generated constructor stub
	}

	@Override
	public Structures findStructuresById(Structures structures) {
		return entityManager.find(Structures.class, structures.getIdStructures());
	}

	@Override
	public List<Structures> findAll() {
		return entityManager.createNamedQuery("Structures.findAll")
				.getResultList();
	}

	@Override
	public void deleteStructures(Structures structures) {
		structures = entityManager.merge(structures);
		entityManager.remove(structures);
	}

	@Override
	public Structures updateStructures(Structures structures) {
		return entityManager.merge(structures);

	}

	@Override
	public Structures createStructures(Structures structures) {
		entityManager.persist(structures);
		return structures;
		
	}

}
















