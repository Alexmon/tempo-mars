package fr.imie.service;

import java.util.List;

import javax.ejb.Local;

import fr.cyberbase.model.Usager;

@Local
public interface IUsagersService {
	
	public abstract List<Usager> getListeUsagers ();
	public abstract Usager createUsagers(Usager u); 
	public abstract Usager findUnUsagerById(Usager u);
	public abstract void deleteUnUsager(Usager u);
	public abstract Usager updateUnUsager(Usager u);
	


}
