package fr.imie.service;

import java.util.ArrayList;
import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import fr.cyberbase.model.Usager;
@Stateless
public class UsagersService implements IUsagersService {

	@PersistenceContext
	EntityManager entityManager;
	
	public UsagersService() {
		// TODO Auto-generated constructor stub
	}

	@Override
	public List<Usager> getListeUsagers() {
		List<Usager> mesUsagers = new ArrayList<Usager>();
		mesUsagers = entityManager.createNamedQuery("Usager.findAll").getResultList();
		return mesUsagers;
	}

	@Override
	public Usager createUsagers(Usager u) {
		entityManager.persist(u);
		return u;
	}

	@Override
	public Usager findUnUsagerById(Usager u) {
		return entityManager.find(Usager.class, u.getIdUsagers());
	}

	@Override
	public void deleteUnUsager(Usager u) {
		u = entityManager.find(Usager.class, u.getIdUsagers());
		u.setVisibleUsagers(false);
		
	}

	@Override
	public Usager updateUnUsager(Usager u) {
		return entityManager.merge(u);
	}

}
