package fr.imie.service;

import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import fr.cyberbase.model.Employe;

@Stateless
public class ServiceEmploye implements IServiceEmploye {
	
	@PersistenceContext
	EntityManager entityManager;
	
	

	public ServiceEmploye() {
		// TODO Auto-generated constructor stub
	}

	@Override
	public Employe findEmployeeById(Employe employeMDE) {
		return entityManager.find(Employe.class, employeMDE.getIdEmploye());
	}
	
	
	public String champTest() {
		String test = "testEJB";
		return test;
		
	}
	
	
	

	@Override
	public List<Employe> findEmployeAll() {
		return entityManager.createNamedQuery("Employe.findAll").getResultList();
	}

	@Override
	public void deleteEmploye(Employe employeMDE) {
		employeMDE = entityManager.merge(employeMDE);
		entityManager.remove(employeMDE);
		
	}

	@Override
	public Employe updatePerson(Employe employeMDE) {
		
		return entityManager.merge(employeMDE);
	}

	@Override
	public Employe createPerson(Employe employeMDE) {
		entityManager.persist(employeMDE);
		return employeMDE;
	}

}
