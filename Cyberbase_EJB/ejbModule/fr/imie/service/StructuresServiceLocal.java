package fr.imie.service;

import java.util.List;

import fr.cyberbase.model.Structures;

public interface StructuresServiceLocal {

	Structures findStructuresById(Structures structures);

	List<Structures> findAll();

	void deleteStructures(Structures structures);

	Structures updateStructures(Structures structures);

	Structures createStructures(Structures structures);

}
