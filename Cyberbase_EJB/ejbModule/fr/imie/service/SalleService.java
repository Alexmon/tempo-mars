package fr.imie.service;

import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import fr.cyberbase.model.Salle;
import fr.cyberbase.model.SallePostes;

/**
 * Session Bean implementation class SalleService
 */
@Stateless
public class SalleService implements SalleServiceLocal {

	@PersistenceContext
	EntityManager entityManager;

	/**
	 * Default constructor.
	 */
	public SalleService() {
		// TODO Auto-generated constructor stub
	}

	@Override
	public Salle findSalleById(Salle salle) {
		return entityManager.find(Salle.class, salle.getIdSalle());
	}
	
	@Override
	public SallePostes findSallePostesById(SallePostes salle) {
		return entityManager.find(SallePostes.class, salle.getIdSalle());
	}
	
	@Override
	public List<Salle> findAll() {
		return entityManager.createNamedQuery("Salle.findAll").getResultList();
	}

	@Override
	public void deleteSalle(Salle salle) {
		salle = entityManager.merge(salle);
		entityManager.remove(salle);
	}

	@Override
	public Salle updateSalle(Salle salle) {
		return entityManager.merge(salle);

	}

	@Override
	public Salle createSalle(Salle salle) {
		entityManager.persist(salle);
		return salle;
		
	}

}
















