package fr.imie.service;

import java.util.List;

import fr.cyberbase.model.NiveauFormation;

public interface NiveauFormationServiceLocal {

	NiveauFormation findNiveauFormationById(NiveauFormation niveauFormation);

	List<NiveauFormation> findAll();

	void deleteNiveauFormation(NiveauFormation niveauFormation);

	NiveauFormation updateNiveauFormation(NiveauFormation niveauFormation);

	NiveauFormation createNiveauFormation(NiveauFormation niveauFormation);

}
