package fr.imie.service;

import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import fr.cyberbase.model.Fonction;

/**
 * Session Bean implementation class FonctionService
 */
@Stateless
public class FonctionService implements FonctionServiceLocal {

	@PersistenceContext
	EntityManager entityManager;

	/**
	 * Default constructor.
	 */
	public FonctionService() {
		// TODO Auto-generated constructor stub
	}

	@Override
	public Fonction findFonctionById(Fonction fonction) {
		return entityManager.find(Fonction.class, fonction.getIdFonction());
	}

	@Override
	public List<Fonction> findAll() {
		return entityManager.createNamedQuery("Fonction.findAll")
				.getResultList();
	}

	@Override
	public void deleteFonction(Fonction fonction) {
		fonction = entityManager.merge(fonction);
		entityManager.remove(fonction);
	}

	@Override
	public Fonction updateFonction(Fonction fonction) {
		return entityManager.merge(fonction);

	}

	@Override
	public Fonction createFonction(Fonction fonction) {
		entityManager.persist(fonction);
		return fonction;
		
	}

}
















