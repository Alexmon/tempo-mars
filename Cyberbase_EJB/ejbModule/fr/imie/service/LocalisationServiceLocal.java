package fr.imie.service;

import java.util.List;

import fr.cyberbase.model.Localisation;

public interface LocalisationServiceLocal {

	Localisation findLocalisationById(Localisation localisation);

	List<Localisation> findAll();

	void deleteLocalisation(Localisation localisation);

	Localisation updateLocalisation(Localisation localisation);

	Localisation createLocalisation(Localisation localisation);

}
