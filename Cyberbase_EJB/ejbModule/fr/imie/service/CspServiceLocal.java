package fr.imie.service;

import java.util.List;

import javax.ejb.Local;

import fr.cyberbase.model.Csp;

@Local
public interface CspServiceLocal {

		List<Csp> findAll();

	void deleteCsp(Csp csp);

	Csp updateCsp(Csp csp);

	Csp createCsp(Csp csp);

	Csp findCspById(Csp csp);

}
