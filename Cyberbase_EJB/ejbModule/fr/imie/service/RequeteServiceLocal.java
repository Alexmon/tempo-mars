package fr.imie.service;

import java.util.List;

import fr.cyberbase.model.Requete;

public interface RequeteServiceLocal {

	Requete findRequeteById(Requete requete);

	List<Requete> findAll();

	void deleteRequete(Requete requete);

	Requete updateRequete(Requete requete);

	Requete createRequete(Requete requete);

}
