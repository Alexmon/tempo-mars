package fr.imie.service;

import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import fr.cyberbase.model.Poste;

/**
 * Session Bean implementation class PosteService
 */
@Stateless
public class PosteService implements PosteServiceLocal {

	@PersistenceContext
	EntityManager entityManager;

	/**
	 * Default constructor.
	 */
	public PosteService() {
		// TODO Auto-generated constructor stub
	}

	@Override
	public Poste findPosteById(Poste poste) {
		return entityManager.find(Poste.class, poste.getIdPoste());
	}

	@Override
	public List<Poste> findAll() {
		return entityManager.createNamedQuery("Poste.findAll")
				.getResultList();
	}
	

	@Override
	public void deletePoste(Poste poste) {
		poste = entityManager.merge(poste);
		entityManager.remove(poste);
	}

	@Override
	public Poste updatePoste(Poste poste) {
		return entityManager.merge(poste);

	}

	@Override
	public Poste createPoste(Poste poste) {
		entityManager.persist(poste);
		return poste;
		
	}


}
















