package fr.imie.service;

import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import fr.cyberbase.model.Csp;

/**
 * Session Bean implementation class CspService
 */
@Stateless
public class CspService implements CspServiceLocal {

	@PersistenceContext
	EntityManager entityManager;

	/**
	 * Default constructor.
	 */
	public CspService() {
		// TODO Auto-generated constructor stub
	}

	@Override
	public Csp findCspById(Csp csp) {
		return entityManager.find(Csp.class, csp.getIdCsp());
	}

	@Override
	public List<Csp> findAll() {
		return entityManager.createNamedQuery("Csp.findAll")
				.getResultList();
	}

	@Override
	public void deleteCsp(Csp csp) {
		csp = entityManager.merge(csp);
		entityManager.remove(csp);
	}

	@Override
	public Csp updateCsp(Csp csp) {
		return entityManager.merge(csp);

	}

	@Override
	public Csp createCsp(Csp csp) {
		entityManager.persist(csp);
		return csp;
		
	}

}
















