package fr.imie.service;

import java.util.List;

import javax.ejb.Local;

import fr.cyberbase.model.Employe;



@Local
public interface IServiceEmploye {
	
	
	public Employe findEmployeeById (Employe employeMDE);
	
	public List<Employe> findEmployeAll();
	
	public String champTest();
	
	public void deleteEmploye (Employe employeMDE);
	
	public Employe updatePerson(Employe employeMDE);
	
	public Employe createPerson(Employe employeMDE);

}
