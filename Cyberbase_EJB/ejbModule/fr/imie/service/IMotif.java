package fr.imie.service;

import java.util.List;

import javax.ejb.Local;

import fr.cyberbase.model.Motif;
import fr.cyberbase.model.Usager;
import fr.cyberbase.model.Utilisation;


@Local
public interface IMotif {
	
	public abstract List<Motif> getAllMotif();
	public abstract List<Utilisation>getAllMotifByUsagers(Usager u);
	public abstract void deleteUnMotif(Motif ut);
	public abstract Motif updateUnMotif (Motif ut);
	public abstract Motif creerUnMotif (Motif ut);
	public abstract Motif findUnMotifById (Motif ut);
}
