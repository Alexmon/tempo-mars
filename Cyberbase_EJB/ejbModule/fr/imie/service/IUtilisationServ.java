package fr.imie.service;

import java.util.List;

import javax.ejb.Local;

import fr.cyberbase.model.Poste;
import fr.cyberbase.model.Usager;
import fr.cyberbase.model.Utilisation;

@Local
public interface IUtilisationServ {
	
	public abstract List<Utilisation> getAllUtilisation();
	public abstract List<Utilisation>getAllUtilisationByUsagers(Usager u);
	public abstract void deleteUneUtilisation(Utilisation ut);
	public abstract Utilisation updateUneUtilisation (Utilisation ut);
	public abstract Utilisation creerUneUtilisation (Utilisation ut);
	public abstract List<Utilisation> getUtilisationPourUnPostePourUneJournee(Poste po);

}
