package fr.imie.service;

import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import fr.cyberbase.model.Requete;

/**
 * Session Bean implementation class RequeteService
 */
@Stateless
public class RequeteService implements RequeteServiceLocal {

	@PersistenceContext
	EntityManager entityManager;

	/**
	 * Default constructor.
	 */
	public RequeteService() {
		// TODO Auto-generated constructor stub
	}

	@Override
	public Requete findRequeteById(Requete requete) {
		return entityManager.find(Requete.class, requete.getIdRequete());
	}

	@Override
	public List<Requete> findAll() {
		return entityManager.createNamedQuery("Requete.findAll")
				.getResultList();
	}

	@Override
	public void deleteRequete(Requete requete) {
		requete = entityManager.merge(requete);
		entityManager.remove(requete);
	}

	@Override
	public Requete updateRequete(Requete requete) {
		return entityManager.merge(requete);

	}

	@Override
	public Requete createRequete(Requete requete) {
		entityManager.persist(requete);
		return requete;
		
	}

}
















