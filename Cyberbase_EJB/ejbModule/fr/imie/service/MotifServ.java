package fr.imie.service;

import java.util.ArrayList;
import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

import fr.cyberbase.model.Motif;
import fr.cyberbase.model.Usager;
import fr.cyberbase.model.Utilisation;

@Stateless
public class MotifServ implements IMotif {
	@PersistenceContext
	EntityManager entityManager;
	
	@Override
	public List<Motif> getAllMotif() {
		List<Motif> mesMotif = new ArrayList<Motif>();
		return mesMotif = entityManager.createNamedQuery("Motif.findAll").getResultList();
	}

	@Override
	public List<Utilisation> getAllMotifByUsagers(Usager u) {
		CriteriaBuilder qb = entityManager.getCriteriaBuilder();
		CriteriaQuery<Utilisation> query = qb.createQuery(Utilisation.class);
		Root<Utilisation> person = query.from(Utilisation.class);
		query.where(qb.equal(person.get("idUsagers"), u.getIdUsagers()));
		List<Utilisation> result = entityManager.createQuery(query).getResultList();
		return result;
	}

	@Override
	public void deleteUnMotif(Motif ut) {
		ut = entityManager.find(Motif.class, ut.getIdMotif());
		entityManager.remove(ut);
		
	}

	@Override
	public Motif updateUnMotif(Motif ut) {
		ut = entityManager.find(Motif.class, ut.getIdMotif());
		return entityManager.merge(ut);
	}

	@Override
	public Motif creerUnMotif(Motif ut) {
		entityManager.persist(ut);
		return ut;
	}

	@Override
	public Motif findUnMotifById(Motif ut) {
		
		return entityManager.find(Motif.class, ut.getIdMotif());
	
	}


}
