package fr.imie.service;

import java.util.List;

import javax.ejb.Local;

import fr.cyberbase.model.Salle;
import fr.cyberbase.model.SallePostes;

@Local
public interface SalleServiceLocal {

		List<Salle> findAll();

	void deleteSalle(Salle salle);

	Salle updateSalle(Salle salle);

	Salle createSalle(Salle salle);

	Salle findSalleById(Salle salle);

	SallePostes findSallePostesById(SallePostes salle);

}
