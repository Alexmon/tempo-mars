package fr.imie.service;

import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import fr.cyberbase.model.NiveauFormation;

/**
 * Session Bean implementation class NiveauFormationService
 */
@Stateless
public class NiveauFormationService implements NiveauFormationServiceLocal {

	@PersistenceContext
	EntityManager entityManager;

	/**
	 * Default constructor.
	 */
	public NiveauFormationService() {
		// TODO Auto-generated constructor stub
	}

	@Override
	public NiveauFormation findNiveauFormationById(NiveauFormation niveauFormation) {
		return entityManager.find(NiveauFormation.class, niveauFormation.getidNivFormation());
	}

	@Override
	public List<NiveauFormation> findAll() {
		return entityManager.createNamedQuery("NiveauFormation.findAll")
				.getResultList();
	}

	@Override
	public void deleteNiveauFormation(NiveauFormation niveauFormation) {
		niveauFormation = entityManager.merge(niveauFormation);
		entityManager.remove(niveauFormation);
	}

	@Override
	public NiveauFormation updateNiveauFormation(NiveauFormation niveauFormation) {
		return entityManager.merge(niveauFormation);

	}

	@Override
	public NiveauFormation createNiveauFormation(NiveauFormation niveauFormation) {
		entityManager.persist(niveauFormation);
		return niveauFormation;
		
	}

}
















