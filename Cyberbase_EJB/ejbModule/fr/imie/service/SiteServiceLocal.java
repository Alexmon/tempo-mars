package fr.imie.service;

import java.util.List;

import javax.ejb.Local;

import fr.cyberbase.model.Site;
import fr.cyberbase.model.SiteSalles;

@Local
public interface SiteServiceLocal {

		List<Site> findAll();

	void deleteSite(Site site);

	Site updateSite(Site site);

	Site createSite(Site site);

	Site findSiteById(Site site);

	SiteSalles findSiteSallesById(SiteSalles site);

}
