package fr.imie.service;

import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import fr.cyberbase.model.SallePostes;
import fr.cyberbase.model.Site;
import fr.cyberbase.model.SiteSalles;

/**
 * Session Bean implementation class SiteService
 */
@Stateless
public class SiteService implements SiteServiceLocal {

	@PersistenceContext
	EntityManager entityManager;

	/**
	 * Default constructor.
	 */
	public SiteService() {
		// TODO Auto-generated constructor stub
	}

	@Override
	public Site findSiteById(Site site) {
		return entityManager.find(Site.class, site.getIdSite());
	}

	@Override
	public List<Site> findAll() {
		return entityManager.createNamedQuery("Site.findAll")
				.getResultList();
	}
	@Override
	public SiteSalles findSiteSallesById(SiteSalles site) {
		return entityManager.find(SiteSalles.class, site.getIdSite());
	}
	
	@Override
	public void deleteSite(Site site) {
		site = entityManager.merge(site);
		entityManager.remove(site);
	}

	@Override
	public Site updateSite(Site site) {
		return entityManager.merge(site);

	}

	@Override
	public Site createSite(Site site) {
		entityManager.persist(site);
		return site;
		
	}

}
















