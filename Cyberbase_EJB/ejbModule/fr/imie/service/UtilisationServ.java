package fr.imie.service;



import java.sql.Date;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Expression;
import javax.persistence.criteria.Root;

import fr.cyberbase.model.Poste;
import fr.cyberbase.model.Usager;
import fr.cyberbase.model.Utilisation;

@Stateless
public class UtilisationServ implements IUtilisationServ {
	
	@PersistenceContext
	EntityManager entityManager;
	@EJB
	private IUsagersService usagerService;

	@Override
	public List<Utilisation> getAllUtilisation() {
		List<Utilisation> lesUtilisations = new ArrayList<Utilisation>();
		lesUtilisations = entityManager.createNamedQuery("Utilisation.findAll").getResultList();
		return lesUtilisations;
	}

	@Override
	public List<Utilisation> getAllUtilisationByUsagers(Usager u) {
		CriteriaBuilder qb = entityManager.getCriteriaBuilder();
		CriteriaQuery<Utilisation> query = qb.createQuery(Utilisation.class);
		Root<Utilisation> person = query.from(Utilisation.class);
		query.where(qb.equal(person.get("idUsagers"), u.getIdUsagers()));
		List<Utilisation> result = entityManager.createQuery(query).getResultList();
		return result;
	}

	@Override
	public void deleteUneUtilisation(Utilisation ut) {
		ut = entityManager.merge(ut);
		entityManager.remove(ut);
		
	}

	@Override
	public Utilisation updateUneUtilisation(Utilisation ut) {
		ut= entityManager.merge(ut);
		
		return ut;
	}
	
	@Override
	public Utilisation creerUneUtilisation(Utilisation ut){

		java.sql.Date DateObject = new java.sql.Date(Calendar.getInstance().getTime().getTime());
		ut.setDateUtilisation(DateObject);
		entityManager.persist(ut);
		return ut;
	}

	@Override
	public List<Utilisation> getUtilisationPourUnPostePourUneJournee(Poste po) {
		
		CriteriaBuilder qb = entityManager.getCriteriaBuilder();
		CriteriaQuery<Utilisation> query = qb.createQuery(Utilisation.class);
		Root<Utilisation> utilisation = query.from(Utilisation.class);
		
		Expression<Date> aujourdhui = qb.currentDate();
		query.where(qb.and( qb.equal(utilisation.get("idPoste"), po.getIdPoste())),qb.equal(utilisation.get("dateUtilisation"), aujourdhui));
		System.out.println(aujourdhui);
	
		List<Utilisation> result = entityManager.createQuery(query).getResultList();
		return result;
	}


	

}
