package fr.imie.service;

import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import fr.cyberbase.model.Localisation;

/**
 * Session Bean implementation class LocalisationService
 */
@Stateless
public class LocalisationService implements LocalisationServiceLocal {

	@PersistenceContext
	EntityManager entityManager;

	/**
	 * Default constructor.
	 */
	public LocalisationService() {
		// TODO Auto-generated constructor stub
	}

	@Override
	public Localisation findLocalisationById(Localisation localisation) {
		return entityManager.find(Localisation.class, localisation.getIdQuartier());
	}

	@Override
	public List<Localisation> findAll() {
		return entityManager.createNamedQuery("Localisation.findAll")
				.getResultList();
	}

	@Override
	public void deleteLocalisation(Localisation localisation) {
		localisation = entityManager.merge(localisation);
		entityManager.remove(localisation);
	}

	@Override
	public Localisation updateLocalisation(Localisation localisation) {
		return entityManager.merge(localisation);

	}

	@Override
	public Localisation createLocalisation(Localisation localisation) {
		entityManager.persist(localisation);
		return localisation;
		
	}

}
















