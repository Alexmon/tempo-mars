package fr.imie.service;

import java.util.List;

import fr.cyberbase.model.Fonction;

public interface FonctionServiceLocal {

	Fonction findFonctionById(Fonction fonction);

	List<Fonction> findAll();

	void deleteFonction(Fonction fonction);

	Fonction updateFonction(Fonction fonction);

	Fonction createFonction(Fonction fonction);

}
